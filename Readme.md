# Tome

A simple, markdown-based wiki, backed by sqlite. It's designed to work for small to medium-sized sites, and meet the 
needs of anything from an individual to a development group. It has a basic, hierarchical page structure, simple user 
management and rules-based permission system.

## Features

TBD.

## Development

TBD.

### Adding Migrations or Seeds

In order to make a new migration file (or seed file), you will need to use `--env` to specify which database you're 
creating it for. So, for example, to add a new migration to the `media` database, simply run the following command:

```bash
$ knex migrate:make some_migration --env media
```

After that, then rename the file to make the number match the conventions in the directory.

## TODO

An initial list of things I need to take care of.

* [ ] Initial Setup
  * [X] Setup Express Server
    * [X] Add Version endpoint
    * [X] Add Dev Server functionality
  * [X] Fix up npm run tasks
  * [ ] Add Socket.io
  * [ ] Add Auth
    * [ ] Local
    * [ ] Google
    * [ ] GitLab
    * [ ] Github
    * [ ] Discord
    * Additional Auth:
        * [ ] Facebook
        * [ ] Microsoft
        * [ ] Twitter
