# Tome Design

This is an attempt to document Tome's design, mainly to help with implementation.

## Wiki Pages

Wiki pages are, conceptually, just some metadata around a markdown document. However, the difficulty comes in 
managing revisions. Because we want to keep the ability to revert to past versions, or undo a delete, we have to 
endure some complexity.

### Models

This is the database model for the main wiki page record:
```typescript
interface WikiPageDB
{
    path : string;
    created : Date;
    latestRevID : string;
}
```

This represents the status of the particular wiki page revision:
```typescript
type WikiStatus = 'draft' | 'published' | 'deleted';
```

This represents the database model for an individual wiki page revision:
```typescript
interface WikiRevDB
{
    revID : string;
    path : string;
    status : WikiStatus;
    title : string;
    headline : string;
    tags : string[];
    contents : string;
    created : Date;
    account : string;
}
```

This is the final composite record for a wiki page:
```typescript
interface WikiPage
{
    baseRevID : string | null;
    path : string;
    status : WikiStatus;
    title : string;
    headline : string;
    tags : string[];
    contents : string;
    created : Date;
    updated : Date;
    account : string;
}
```

### Page API Design

#### HEAD `/api/page/<path>`

* `200` - The page exists.
* `403` - The user doesn't have permissions.
* `404` - The page does not exist.

Used to determine if a page exists.

#### GET `/api/page/<path>`

* `200` - The page exists.
* `403` - The user doesn't have permissions.
* `404` - The page does not exist.

This retrieves a single wiki page from the database, and returns a `WikiPage` model.

##### Details

We use the `latestRevID` from the `WikiPageDB` model to retrieve the right `WikiRevDB` and mix it in to build the 
`WikiPage`.

#### PUT `/api/page/<path>`

* `200` - The page was added/updated successfully.
* `403` - The user doesn't have permissions.
* `409` - The page has been updated since last modified.

This either adds (or updates, adding a new revision) a wiki page, taking a `WikiPage` model record in JSON. (It does not check to see if the contents have changed; every time it is called, a new revision will be created.)

If the page has been updated in between pulling the contents and editing, returns it returns a `409` 
to allow the user the chance to fix any conflicts. (If the changes can be applied, the client should automatically 
resubmit.)

#### DELETE `/api/page/<path>`

* `200` - The page was updated successfully.
* `403` - The user doesn't have permissions.

Deletes the specified wiki page. Can take a `purge` property that, if true, actually deletes the page and all 
revisions from the database.

##### Details

Inserts a `deleted` status revision if the latest revision isn't already a `deleted` revision. If the page doesn't 
exist, we still return `200`.

### Revision API Design

#### GET `/api/page/<path>/revisions`

* `200` - The page exists with revisions.
* `403` - The user doesn't have permissions.
* `404` - The page does not exist.

Returns a list of all revisions for the given path, if any. If the page doesn't exist, return a `404`.

#### POST `/api/page/<path>/prune`

* `200` - The page revisions were pruned.
* `403` - The user doesn't have permissions.
* `404` - The page does not exist.

Deletes all prior revisions, other than the current one. If the latest revision has a status of `'deleted'`, the 
page is purged.

#### POST `/api/page/<path>/revert/<revID>`

* `200` - The page was reverted successfully.
* `403` - The user doesn't have permissions.
* `404` - The page does not exist.
* `422` - The revID does not exist.

Reverts a page's latest revision to the specified `revID`. If the `revID` doesn't exist, it returns a `422`. If the 
page doesn't exist, it returns a `404`.

## Media

Media files exist primarily to allow images and other media to be uploaded, so they can be used in wiki pages. The 
concept used is simple: we replicate filesystem storage in the database, so there's no security issues. We allow the creation. deletion, and renaming of files. In short, this is just a easy way to upload/manage files for use on the wiki.

A special static server is implemented to read from the media database and serve files with the correct content type.

### Models

This represents a media item. The `mime` property `inode/directory` is used for folders, otherwise it's the correct mime type for the filesystem. The data property is null for directories.

```typescript
interface MediaItem
{
    path : string;
    name : string;
    mime : string;
    data : Buffer | null;
}
```

This is the same model as above, just with the `data` property removed, so it can be used as a summary, without the data size penalty.

```typescript
interface MediaMetadata
{
    path : string;
    name : string;
    mime : string;
}
```

### Media API Design

#### GET `/api/media/<path>`

* `200` - The media item exists.
* `403` - The user doesn't have permissions.
* `404` - The media does not exist.

Returns either a `MediaMetadata` or a list of `MediaMetadata[]`, depending on if the path is a directory or a file. If it doesn't exist, a `404` is returned.

#### PUT `/api/media/<path>`

* `200` - The media item was successfully uploaded.
* `403` - The user doesn't have permissions.
* `404` - The media does not exist.
* `409` - A media item exists, and it cannot be overwritten due to conflicts.

_Note: This is a multipart/form-data upload!_

Adds or updates a media item, taking a `MediaItem` in JSON, as well as the file as a multipart form, optionally. If the item is a folder, and non-folder is `PUT` over top of it, a `409` is returned, as that is not allowed.

#### PATCH `/api/media/<path>`
* `200` - The media item was successfully moved.
* `403` - The user doesn't have permissions.
* `404` - The media does not exist.
* `409` - A media item of this name exists.

Renames a media item. If an item of the same name exists in the current path, returns a `409`.

#### POST `/api/media/<path>/move/<newPath>`

* `200` - The media item was successfully moved.
* `403` - The user doesn't have permissions.
* `404` - The media does not exist.
* `409` - A media item cannot be moved due to conflicts.

Moves a media item from one path to another. The same rules as `PUT` apply for returning a `409`.

#### DELETE `/api/media/<path>`

* `200` - The media item was successfully deleted.
* `403` - The user doesn't have permissions.
* `409` - The media item can't be deleted.

Deletes the media item. A `409` is returned if the media item is a folder, and the folder has contents. Supports a `force` query parameter to delete the entire directory, even with contents.
