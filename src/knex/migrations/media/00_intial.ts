// ---------------------------------------------------------------------------------------------------------------------
// Initial Migration
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from "knex";

// ---------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex): Promise<void>
{
    await knex.schema.createTable('media_item', (table) =>
    {
        table.text('dir').notNullable();
        table.text('name').notNullable().index();
        table.text('mime').notNullable();
        table.binary('data');
        table.primary(['dir', 'name']);
    });
}

// ---------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex): Promise<void>
{
    await knex.schema.dropTable('media_item');
}

// ---------------------------------------------------------------------------------------------------------------------
