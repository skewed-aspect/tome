// ---------------------------------------------------------------------------------------------------------------------
// Migration
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from "knex";

// ---------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex): Promise<void>
{
    await knex.schema.createTable('wiki_rev', (table) =>
    {
        table.text('rev_id').primary();
        table.text('path').notNullable();
        table.enum('status', [ 'draft', 'published', 'deleted' ]);
        table.text('title').notNullable();
        table.text('headline').notNullable().defaultTo('');
        table.json('tags').notNullable().defaultTo('[]');
        table.text('contents').notNullable().defaultTo('');
        table.timestamp('created').notNullable().defaultTo(knex.fn.now());
        table.text('account').notNullable();
    });

    await knex.schema.createTable('wiki_page', (table) =>
    {
        table.text('path').primary();
        table.timestamp('created').notNullable().defaultTo(knex.fn.now());
        table.string('latest_rev').notNullable();
        table.foreign('latest_rev').references('wiki_rev.rev_id')
            .onDelete('CASCADE')
            .onUpdate('CASCADE');
    });
}

// ---------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex): Promise<void>
{
    await knex.schema.dropTable('wiki_page');
    await knex.schema.dropTable('wiki_rev');
}

// ---------------------------------------------------------------------------------------------------------------------
