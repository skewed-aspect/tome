// ---------------------------------------------------------------------------------------------------------------------
// Migration
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from "knex";

// ---------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex): Promise<void>
{
    await knex.schema.createTable('account', (table) =>
    {
        table.text('email').primary();
        table.string('name', 255);
        table.text('avatar');
        table.json('settings').notNullable().defaultTo('{}');
    });

    await knex.schema.createTable('reset_token', (table) =>
    {
        table.increments('id').primary().unsigned();
        table.text('email').notNullable().index();
        table.string('hash', 60).notNullable();
        table.timestamp('created').notNullable().defaultTo(knex.fn.now());
        table.foreign('email').references('account.email')
            .onDelete('CASCADE')
            .onUpdate('CASCADE');
    });

    await knex.schema.createTable('auth_local', (table) =>
    {
        table.increments('id').primary().unsigned();
        table.text('email').notNullable().unique();
        table.string('hash', 60).notNullable();
        table.text('avatar');
        table.boolean('confirmed').notNullable().defaultTo(false);
        table.foreign('email').references('account.email')
            .onDelete('CASCADE')
            .onUpdate('CASCADE');
    });

    await knex.schema.createTable('auth_discord', (table) =>
    {
        table.increments('id').primary().unsigned();
        table.text('email').notNullable().unique();
        table.text('avatar');
        table.foreign('email').references('account.email')
            .onDelete('CASCADE')
            .onUpdate('CASCADE');
    });

    await knex.schema.createTable('auth_github', (table) =>
    {
        table.increments('id').primary().unsigned();
        table.text('email').notNullable().unique();
        table.text('avatar');
        table.foreign('email').references('account.email')
            .onDelete('CASCADE')
            .onUpdate('CASCADE');
    });

    await knex.schema.createTable('auth_gitlab', (table) =>
    {
        table.increments('id').primary().unsigned();
        table.text('email').notNullable().unique();
        table.text('avatar');
        table.foreign('email').references('account.email')
            .onDelete('CASCADE')
            .onUpdate('CASCADE');
    });

    await knex.schema.createTable('auth_google', (table) =>
    {
        table.increments('id').primary().unsigned();
        table.text('email').notNullable().unique();
        table.text('avatar');
        table.foreign('email').references('account.email')
            .onDelete('CASCADE')
            .onUpdate('CASCADE');
    });
}

// ---------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex): Promise<void>
{
    await knex.schema.dropTable('account');
    await knex.schema.dropTable('auth_local');
    await knex.schema.dropTable('auth_discord');
    await knex.schema.dropTable('auth_github');
    await knex.schema.dropTable('auth_gitlab');
    await knex.schema.dropTable('auth_google');
}

// ---------------------------------------------------------------------------------------------------------------------
