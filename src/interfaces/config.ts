// ---------------------------------------------------------------------------------------------------------------------
// Configuration Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';
import { LoggerConfig } from '@strata-js/util-logging';

import { XOR } from 'ts-essentials';

// ---------------------------------------------------------------------------------------------------------------------

export interface LocalAuthConfig
{
    saltRounds ?: number;
    skipRegistrationEmail ?: boolean;
}

export interface AuthConfig
{
    local ?: LocalAuthConfig;
}

export interface FakeMailConfig {
    testingMode : boolean;
}

export interface SMTPMailConfig {
    host : string;
    port : number;
    secure : boolean;
    auth ?: {
        user : string;
        pass : string;
    }
}

export type MailConfig = XOR<FakeMailConfig, SMTPMailConfig>;

export interface SessionsConfig
{
    table ?: string;
    sid ?: string;
    secret : string;
}

export interface DatabasesConfig
{
    media: Knex.Config;
    wiki: Knex.Config;
}

export interface HTTPServerConfig
{
    devMode: boolean;
    port: number;
}

export interface TomeConfig
{
    auth : AuthConfig;
    mail ?: MailConfig;
    server : HTTPServerConfig;
    session : SessionsConfig;
    logging ?: LoggerConfig;
    databases : DatabasesConfig;
}

// ---------------------------------------------------------------------------------------------------------------------
