// ---------------------------------------------------------------------------------------------------------------------
// Settings Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export interface SiteSettings
{
    name : string;
    description : string;
    url : string;
    icon : string;
}

export interface SettingRecord<T extends Record<string, unknown> = Record<string, unknown>>
{
    name : string;
    details : T;
}

// ---------------------------------------------------------------------------------------------------------------------
