// ---------------------------------------------------------------------------------------------------------------------
// Account Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export interface AccountSettings
{
    // TODO: Come up with something to go here.
}

export interface Account
{
    email : string;
    name : string | null;
    avatar : string | null;
    settings : AccountSettings
}

// ---------------------------------------------------------------------------------------------------------------------

export interface BaseProvider
{
    id ?: number;
    email : string;
    avatar ?: string | null;
}

export interface LocalProvider extends BaseProvider
{
    kind : 'local';
    hash : string;
}

export interface DiscordProvider extends BaseProvider
{
    kind : 'discord';
}

export interface GitHubProvider extends BaseProvider
{
    kind : 'github';
}

export interface GitLabProvider extends BaseProvider
{
    kind : 'gitlab';
}

export interface GoogleProvider extends BaseProvider
{
    kind : 'google';
}

export type AuthProvider = DiscordProvider | GitHubProvider | GitLabProvider | GoogleProvider | LocalProvider;

export const availableProviders = [ 'local', 'discord', 'github', 'gitlab', 'google' ] as const;
export type ProviderKinds = typeof availableProviders[number];

// ---------------------------------------------------------------------------------------------------------------------
