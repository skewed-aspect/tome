// ---------------------------------------------------------------------------------------------------------------------
// Wiki Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export type WikiStatus = 'draft' | 'published' | 'deleted';

export interface WikiRev
{
    revID : string;
    path : string;
    status : WikiStatus;
    title : string;
    headline : string;
    tags : string[];
    contents : string;
    created : number;
    account : string;
}

export interface WikiPage
{
    baseRevID : string | null;
    path : string;
    status : WikiStatus;
    title : string;
    headline : string;
    tags : string[];
    contents : string;
    created : number;
    updated : number;
    account : string;
}

// ---------------------------------------------------------------------------------------------------------------------
