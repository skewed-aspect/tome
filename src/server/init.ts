// ---------------------------------------------------------------------------------------------------------------------
// Server Initialization
// ---------------------------------------------------------------------------------------------------------------------

import fs from 'node:fs';

import dotenv from 'dotenv';
import helmet from 'helmet';
import cookieParser from 'cookie-parser';
import express, { Request, Response } from 'express';
import 'express-async-errors';
import Session from 'express-session';
import knexSessionFactory from 'connect-session-knex';
import passport from 'passport';
import { logging } from '@strata-js/util-logging';

// Configuration
import { TomeConfig } from '../interfaces/config';
import configUtil from './utils/config';
import path from 'path';

// Routes
import AuthRouter from './routes/auth';
import SettingsRouter from './routes/settings';
import VersionRoute from './routes/version';
import WikiRouter from './routes/wiki';

// Middleware
import { requestLogger } from './middleware/logger';

// Resource Access
import emailRA from './resource-access/email';

// Utils
import dbUtil from './utils/database';
import { errorHandler } from './middleware/errorHandler';

//----------------------------------------------------------------------------------------------------------------------
// Configuration
//----------------------------------------------------------------------------------------------------------------------

dotenv.config();
configUtil.loadConfigFile();
const config = configUtil.getConfig<TomeConfig>();

//----------------------------------------------------------------------------------------------------------------------
// Middleware
//----------------------------------------------------------------------------------------------------------------------

const KnexSession = knexSessionFactory(Session);

//----------------------------------------------------------------------------------------------------------------------
// Server
//----------------------------------------------------------------------------------------------------------------------

export async function init(unitTest = false)
{
    // Logging
    logging.setConfig(config?.logging ?? { prettyPrint: false });
    const logger = logging.getLogger('server');

    // Initialize the database
    logger.info('Initializing databases...')
    await dbUtil.initDB('media', unitTest);
    await dbUtil.initDB('wiki', unitTest);

    // Initialize Resource Access
    await emailRA.init();

    // Path to dist folder
    const distPath = path.resolve(__dirname, '..');

    // Express app
    const app = express();

    // Basic security fixes
    app.use(helmet({
        contentSecurityPolicy: false,
        crossOriginEmbedderPolicy: false // This might be useful to enable, but skip it for now
    }));

    // Body support
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    // Logging Middleware
    app.use(requestLogger(logger));

    // Session Storage
    const store = new KnexSession({
        tablename: config.session?.table,
        sidfieldname: config.session?.sid,
        knex: await dbUtil.getDB('wiki'),
        createtable: true,

        // Clear expired sessions. (1 hour)
        clearInterval: 60 * 60 * 1000
    });

    app.use(Session({
        secret: config.session.secret,
        resave: false,
        store,

        // maxAge = 7 days
        cookie: { maxAge: 7 * 24 * 60 * 60 * 1000, secure: true },
        saveUninitialized: false
    }));

    // Auth Middleware
    app.use(cookieParser());

    // Passport support
    app.use(passport.initialize());
    app.use(passport.session());

    // Routes
    app.use('/auth', AuthRouter);
    app.use('/settings', SettingsRouter);
    app.use('/version', VersionRoute);
    app.use('/', WikiRouter);

    // Static Hosting
    app.use(express.static(path.join(distPath, 'client')));

    // Serve index.html for any html requests, but 404 everything else.
    app.get('*', (_request : Request, response : Response) =>
    {
        response.format({
            html: (_req : Request, resp : Response) =>
            {
                resp.setHeader('Content-Type', 'text/html');
                fs.createReadStream(path.join(distPath, 'client', 'index.html')).pipe(resp);
            },
            json: (_req : Request, resp : Response) =>
            {
                resp.status(404).end();
            }
        });
    });

    // Error Handler
    app.use(errorHandler(logger));

    // Return the express app.
    return app;
}

// ---------------------------------------------------------------------------------------------------------------------
