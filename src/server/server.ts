// ---------------------------------------------------------------------------------------------------------------------
// Tome Server
// ---------------------------------------------------------------------------------------------------------------------

import { AddressInfo } from 'node:net';
import { createServer } from 'vite';
import { logging } from '@strata-js/util-logging';

// Configuration
import { TomeConfig } from '../interfaces/config';
import configUtil from './utils/config';

// Router Init
import { init } from './init';

//----------------------------------------------------------------------------------------------------------------------
// Configuration
//----------------------------------------------------------------------------------------------------------------------

const config = configUtil.getConfig<TomeConfig>();

//----------------------------------------------------------------------------------------------------------------------
// Logging
//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('server');

//----------------------------------------------------------------------------------------------------------------------
// Server
//----------------------------------------------------------------------------------------------------------------------

export async function main()
{
    // Initialize the server
    const app = await init();

    // Run server
    if(config.server.devMode)
    {
        // Start the API server on the port before the configured port
        const server = await app.listen(config.server.port - 1);

        // Pull the API server's address info
        const apiServerInfo = server.address() as AddressInfo;

        // Start the vite dev server
        const viteServer = await createServer();
        await viteServer.listen();

        // Pull the API server's address info
        const devServerInfo = viteServer.httpServer?.address() as AddressInfo;

        // Log URLs
        const apiHost = apiServerInfo.address === '::' ? 'localhost' : apiServerInfo.address;
        const devHost = devServerInfo.address === '::1' ? 'localhost' : devServerInfo.address;

        logger.warn('Server is running in devMode.');
        logger.debug(`Tome API Server listening at http://${ apiHost }:${ apiServerInfo.port }.`);
        logger.info(`Tome Server listening at http://${ devHost }:${ devServerInfo.port }.`);
    }
    else
    {
        // Start the API server
        const server = await app.listen(config.server.port);

        // Pull the API server's address info
        const { address, port } = server.address() as AddressInfo;

        // Log the API server's address
        const host = address === '::' ? 'localhost' : address;
        logger.info(`Tome Server listening at http://${ host }:${ port }.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

main()
    .catch((err) =>
    {
        console.error(err.stack);
        process.exit(1);
    });

// ---------------------------------------------------------------------------------------------------------------------
