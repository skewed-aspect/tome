// ---------------------------------------------------------------------------------------------------------------------
// Version Route
// ---------------------------------------------------------------------------------------------------------------------

import express from 'express';

// Utils
import { getGitInfo } from '../utils/git';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();

//----------------------------------------------------------------------------------------------------------------------

router.get('/', async (req, resp) =>
{
    let version = '';
    const env = process.env.ENVIRONMENT ?? 'local';
    const buildVersion = process.env.BUILD_VERSION ?? 'edge';

    const gitInfo = await getGitInfo();

    version = `${ buildVersion }.${ gitInfo.shortHash }`;

    if(env === 'local')
    {
        version += `-${ gitInfo.branch }`;
    }

    resp.json({
        name: 'Tome',
        version,
        environment: env,
        build: {
            commitHash: gitInfo.shortHash,
            commitRef: gitInfo.branch,
            date: gitInfo.commitDate,
            release: buildVersion
        }
    });
});

//----------------------------------------------------------------------------------------------------------------------

export default router;

// ---------------------------------------------------------------------------------------------------------------------
