// ---------------------------------------------------------------------------------------------------------------------
// Settings Route
// ---------------------------------------------------------------------------------------------------------------------

import express from 'express';

// Managers
import settingsMan from '../managers/settings/';

// Utils
import { ensureAuthenticated } from '../utils/routeUtils';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();

//----------------------------------------------------------------------------------------------------------------------

router.get('/', async (req, resp) =>
{
    resp.json(await settingsMan.list());
});

router.get('/:name', async (req, resp) =>
{
    resp.json(await settingsMan.get(req.params.name));
});

router.post('/:name', ensureAuthenticated, async (req, resp) =>
{
    resp.json(await settingsMan.set(req.params.name, req.body));
});

//----------------------------------------------------------------------------------------------------------------------

export default router;

// ---------------------------------------------------------------------------------------------------------------------
