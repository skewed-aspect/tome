// ---------------------------------------------------------------------------------------------------------------------
// Version Route
// ---------------------------------------------------------------------------------------------------------------------

import express from 'express';
import logging from '@strata-js/util-logging';

// Managers
import wikiMan from '../managers/wiki';

// Middleware
import { errorHandler } from '../middleware/errorHandler';
import { NotFoundError, TomeHTTPError } from '../errors';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();
const logger = logging.getLogger('wiki router');

//----------------------------------------------------------------------------------------------------------------------

router.head(':path([A-Za-z0-9_/]*)', async (req, resp) =>
{
    const exists = await wikiMan.exists(req.params.path);
    if(exists)
    {
        resp.end();
    }
    else
    {
        resp.status(404).end();
    }
});

router.get(':path([A-Za-z0-9_/]*)', async (req) =>
{
    const includeDrafts = (req.query['drafts'] ?? '') === 'true';
    return wikiMan.get(req.params.path, includeDrafts)
        .catch((error) =>
        {
            if(error instanceof NotFoundError)
            {
                throw new TomeHTTPError(error.message, error.code, 404);
            }

            throw error;
        });
});

//----------------------------------------------------------------------------------------------------------------------

router.use(errorHandler(logger));

//----------------------------------------------------------------------------------------------------------------------

export default router;

// ---------------------------------------------------------------------------------------------------------------------
