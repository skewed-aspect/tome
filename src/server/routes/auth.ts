// ---------------------------------------------------------------------------------------------------------------------
// Auth Router
// ---------------------------------------------------------------------------------------------------------------------

import express from 'express';
import passport from 'passport';
import rateLimit from 'express-rate-limit'

// Passport Setup
import '../auth/serialization';

// Strategies
import '../auth/local';
import accountMan from '../managers/account';
import { MissingRequiredParameterError } from '../errors';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();

// Set up a rate limit for local account stuff
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000,
    max: 100,
    standardHeaders: true,
    legacyHeaders: false,
});

//----------------------------------------------------------------------------------------------------------------------
// Local Auth Endpoints
//----------------------------------------------------------------------------------------------------------------------


router.post('/local', passport.authenticate('local'), (req, resp) =>
{
    resp.json(req.user);
});

router.post('/local/register', limiter, async (req, resp) =>
{
    const { email, password, name, avatar } = req.body;
    await accountMan.registerLocalAccount(email, password, avatar, name);
    resp.end();
});

router.get('/local/confirm', limiter, async (req, resp) =>
{
    const { token, email } = req.query;
    if(!email || typeof email !== 'string')
    {
        throw new MissingRequiredParameterError('email');
    }

    if(!token || typeof token !== 'string')
    {
        throw new MissingRequiredParameterError('token');
    }

    const valid = await accountMan.confirmEmail(email, token);
    resp.send({ valid }).end();
});

router.post('/local/reset', limiter, async (req, resp) =>
{
    const { email } = req.query;
    if(!email || typeof email !== 'string')
    {
        throw new MissingRequiredParameterError('email');
    }

    await accountMan.sendPasswordResetToken(email);
    resp.end();
});

router.post('/local/reset/confirm', limiter, async (req, resp) =>
{
    const { token, email, password } = req.body;
    if(!email || typeof email !== 'string')
    {
        throw new MissingRequiredParameterError('email');
    }

    if(!token || typeof token !== 'string')
    {
        throw new MissingRequiredParameterError('token');
    }

    if(!password || typeof password !== 'string')
    {
        throw new MissingRequiredParameterError('password');
    }

    const valid = await accountMan.verifyToken(email, token);

    if(valid)
    {
        return accountMan.setPassword(email, password);
    }
    resp.send({ valid }).end();
});

//----------------------------------------------------------------------------------------------------------------------

// Logout endpoint
router.post('/logout', (req, resp, done) =>
{
    req.logout(done);
});

// ---------------------------------------------------------------------------------------------------------------------

export default router;

// ---------------------------------------------------------------------------------------------------------------------
