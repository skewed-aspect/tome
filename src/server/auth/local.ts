// ---------------------------------------------------------------------------------------------------------------------
// Local Passport Strategy
// ---------------------------------------------------------------------------------------------------------------------

import passport from 'passport';
import { IVerifyOptions, Strategy } from 'passport-local';
import { logging } from '@strata-js/util-logging';

// Managers
import accountMan from '../managers/account';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('local auth');

// ---------------------------------------------------------------------------------------------------------------------

async function localHandler(
    email : string,
    password : string,
    done : (error: any, user?: any, options?: IVerifyOptions) => void
) : Promise<void>
{
    try
    {
        const account = await accountMan.get(email);
        if(account)
        {
            const valid = await accountMan.verifyPassword(email, password);
            if(valid)
            {
                done(null, account);
            }
            else
            {
                done(null, false);
            }
        }
        else
        {
            logger.debug(`Account '${ email }' does not exist.`);
            done(null, false);
        }
    }
    catch(e)
    {
        done(e);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

passport.use(new Strategy({ usernameField: 'email' }, localHandler));

// ---------------------------------------------------------------------------------------------------------------------
