//----------------------------------------------------------------------------------------------------------------------
// Handles user serialization/deserialization
//----------------------------------------------------------------------------------------------------------------------

import passport from 'passport';

// Interfaces
import { Account } from '../../interfaces/account';

// Managers
import accountMan from '../managers/account';

//----------------------------------------------------------------------------------------------------------------------

function serialize({ email } : Account, done : (err: any, id?: unknown) => void)
{
    done(null, email);
}

async function deserialize(email : string, done : (err: any, user?: (false | Express.User | null | undefined)) => void)
{
    try
    {
        const account = await accountMan.get(email);
        done(null, account);
    }
    catch (error)
    {
        done(error);
    }
}

//----------------------------------------------------------------------------------------------------------------------

passport.serializeUser(serialize);
passport.deserializeUser(deserialize);

//----------------------------------------------------------------------------------------------------------------------
