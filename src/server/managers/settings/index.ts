//----------------------------------------------------------------------------------------------------------------------
// SettingsManager
//----------------------------------------------------------------------------------------------------------------------

// Interfaces
import { SettingRecord, SiteSettings } from '../../../interfaces/settings';

// Initial Settings
import defaultSettings from './defaults';

// Resource Access
import settingsRA from '../../resource-access/settings';

//----------------------------------------------------------------------------------------------------------------------

class SettingsManager
{
    async getSiteSettings() : Promise<SiteSettings>
    {
        const site = await settingsRA.get<SiteSettings>('site');
        return site ?? defaultSettings.site;
    }

    async get<T>(name : string, defaultVal ?: T) : Promise<T | undefined>
    {
        return settingsRA.get<T>(name, defaultVal);
    }

    async list() : Promise<SettingRecord[]>
    {
        return settingsRA.list();
    }

    async set<T>(name : string, details : T) : Promise<void>
    {
        return settingsRA.set<T>(name, details);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new SettingsManager();

//----------------------------------------------------------------------------------------------------------------------
