// ---------------------------------------------------------------------------------------------------------------------
// Wiki Manager
// ---------------------------------------------------------------------------------------------------------------------

// Interfaces
import { WikiPage, WikiRev } from '../../interfaces/wiki';

// Resource Access
import wikiRA from '../resource-access/wiki';

// ---------------------------------------------------------------------------------------------------------------------

// TODO: Add in authentication and permission checking!

class WikiManager
{
    /**
     * Checks to see if a page exists. A page is defined as existing if it has a wiki_page record and the most recent,
     * non-draft revision has the status 'published'.
     *
     * @param path - the path to the wiki page.
     *
     * @returns Returns true if the most recent non-draft version is published.
     */
    async exists(path : string) : Promise<boolean>
    {
        return wikiRA.exists(path);
    }

    /**
     * Gets a wiki page. Throws a `NotFoundError` if the page doesn't exist.
     *
     * @param path - the path of the wiki page.
     * @param includeDrafts - if true, get the latest revision, even if it's a draft.
     *
     * @returns Returns a `WikiPage` instance.
     */
    async get(path : string, includeDrafts = false) : Promise<WikiPage>
    {
        return wikiRA.get(path, includeDrafts);
    }

    /**
     * Gets a list of all the wiki revisions for a particular page. Throws a `NotFoundError` if no page exists.
     *
     * @param path - the path of the wiki page.
     *
     * @returns Returns a list of `WikiRevDB` items.
     */
    async getRevisions(path : string) : Promise<WikiRev[]>
    {
        return wikiRA.getRevisions(path);
    }

    /**
     * Adds or updates a wiki page. This always adds a new revision, and then sets the latest revision to that.
     *
     * @param path - the path of the wiki page.
     * @param page - the new page contents.
     * @param account - the account that made the edit.
     *
     * @returns Returns the new revision id that was generated.
     */
    async set(
        path : string,
        page : Omit<WikiPage, 'path' | 'created' | 'updated' | 'account'>, account : string = ''
    ) : Promise<string>
    {
        return wikiRA.set(path, page);
    }

    /**
     * Discards the current draft, if any, and all draft revisions newer than the current published revision.
     *
     * @param path - the path of the wiki page.
     *
     * @returns Always returns void.
     */
    async discardDrafts(path : string) : Promise<void>
    {
        return wikiRA.discardDrafts(path);
    }

    /**
     * Deletes a wiki page. If `purge` is passed, it actually deletes the page, otherwise it adds a revision that marks
     * the page as deleted.
     *
     * @param path - the path of the wiki page.
     * @param account - the account that made the edit.
     * @param purge - if true, permanently deletes the page.
     *
     * @returns Always returns void.
     */
    async remove(path : string, account: string = '', purge = false) : Promise<void>
    {
        return wikiRA.remove(path);
    }

    /**
     * Prunes a page's history, collapsing it down to just the latest version. If `discard` is true, all drafts will be
     * discarded. If `purge` is true, purges deleted pages. There will only be a single revision left for the page, or
     * it will be deleted (in the case of `purge`).
     *
     * @param path - the path of the wiki page.
     * @param discard - if true, discard any drafts
     * @param purge - if true, permanently deletes the page.
     *
     * @returns Always returns void.
     */
    async prune(path : string, discard = true, purge = true) : Promise<void>
    {
        return wikiRA.prune(path, discard, purge);
    }

    /**
     * Revert copies a past page revision and makes the copy the latest revision.
     *
     * @param path - the path of the wiki page.
     * @param revID - the revision id of the wiki revision to revert to.
     * @param account - the account that made the edit.
     *
     * @returns Returns the new revision ID.
     */
    async revert(path : string, revID : string, account : string = '') : Promise<string>
    {
        return wikiRA.revert(path, revID, account);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new WikiManager();

// ---------------------------------------------------------------------------------------------------------------------
