// ---------------------------------------------------------------------------------------------------------------------
// Account Manager
// ---------------------------------------------------------------------------------------------------------------------

// Interfaces
import { Account, AuthProvider, availableProviders, ProviderKinds } from '../../interfaces/account';

// Managers
import settingsMan from './settings';

// Resource Access
import accountRA from '../resource-access/account';
import emailRA from '../resource-access/email';
import localRA from '../resource-access/local';

// ---------------------------------------------------------------------------------------------------------------------

class AccountManager
{
    async get(email : string) : Promise<Account | undefined>
    {
        return accountRA.get(email);
    }

    async getProvider(email : string, provider : ProviderKinds) : Promise<AuthProvider | undefined>
    {
        return accountRA.getProvider(email, provider);
    }

    async listProviders(email : string) : Promise<AuthProvider[]>
    {
        const providers = await Promise.all(availableProviders.map(async (provider) =>
        {
            return accountRA.getProvider(email, provider);
        }));

        // Filter out any undefined values
        return providers.filter((prov) => !!prov) as AuthProvider[];
    }

    async setAccount(account : Account) : Promise<void>
    {
        return accountRA.setAccount(account);
    }

    async setProvider(provider : AuthProvider) : Promise<void>
    {
        return accountRA.setProvider(provider);
    }

    async remove(email : string) : Promise<void>
    {
        return accountRA.remove(email);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Local Account Utility Functions
    // -----------------------------------------------------------------------------------------------------------------

    async sendRegistrationEmail(from : string, to : string, tokenUrl : string) : Promise<void>
    {
        const site = await settingsMan.getSiteSettings();
        const text = `Confirm your email on ${ site.name }.\n\nHello! Once you've confirmed that '${ to }' is your `
            + `email address, you can get started. Go to the following url to confirm:\n\n${ tokenUrl }\n\n`
            + `This link will expire in about two hours.\n`
            + `If you didn't request this email, there's nothing you need to do.`;

        const html = `<h1>Confirm your email on ${ site.name }</h1>`
            + `<p>`
            + `Hello! Once you've confirmed that '${ to }' is your email address, you can get started. `
            + `Click the link below to confirm.`
            + `</p>`
            + `<p><a href="${ tokenUrl }" target="_blank">Confirm Email</a></p>`
            + `<p>This link will expire in about two hours.</p>`
            + `<p>If you didn't request this email, there's nothing you need to do.</p>`

        // Send Email
        await emailRA.sendMail(from, to, `Confirm ${ to } on ${ site.name }`, text, html);
    }

    async sendResetEmail(from : string, to : string, tokenUrl : string) : Promise<void>
    {
        const site = await settingsMan.getSiteSettings();
        const text = `A password reset was requested for your account on ${ site.name }.\n\nHello! A password reset`
            + ` was requested for the account '${ to }'. If you requested this, please go to the following `
            + `link:\n\n${ tokenUrl }\n\n`
            + `This link will expire in about two hours.\n`
            + `If you didn't request this email, there's nothing you need to do.`;

        const html = `<h1>Password reset requested on ${ site.name }</h1>`
            + `<p>`
            + `Hello! A password reset was requested for the account '${ to }'. `
            + `If you requested this, please click the following link:`
            + `</p>`
            + `<p><a href="${ tokenUrl }" target="_blank">Confirm Email</a></p>`
            + `<p>This link will expire in about two hours.</p>`
            + `<p>If you didn't request this email, there's nothing you need to do.</p>`

        // Send Email
        await emailRA.sendMail(from, to, `Password Reset requested for ${ to } on ${ site.name }`, text, html);
    }

    async verifyPassword(email : string, password : string) : Promise<boolean>
    {
        return localRA.verifyPassword(email, password);
    }

    async setPassword(email : string, password : string) : Promise<boolean>
    {
        return localRA.verifyPassword(email, password);
    }

    public async sendPasswordResetToken(email : string) : Promise<void>
    {
        const site = await settingsMan.getSiteSettings();

        // Generate a reset token
        const token = await localRA.generateResetToken(email);

        // Build the token url
        const tokenUrl = `${ site.url }/auth/local/resetPassword?token=${ token }&email=${ encodeURIComponent(email) }`;

        // TODO: Figure out how to get the proper 'from' email.
        await this.sendResetEmail('no-reply@example.com', email, tokenUrl);
    }

    async registerLocalAccount(email : string, password : string, avatar ?: string, name ?: string) : Promise<void>
    {
        const site = await settingsMan.getSiteSettings();
        const account = {
            email,
            name : name ?? null,
            avatar : avatar ?? null,
            settings : {}
        };

        await this.setAccount(account);
        await localRA.setPassword(email, password);

        // Generate a reset token
        const token = await localRA.generateResetToken(email);

        // Build the token url
        const tokenUrl = `${ site.url }/auth/local/confirm?token=${ token }&email=${ encodeURIComponent(email) }`;

        // TODO: Figure out how to get the proper 'from' email.
        await this.sendRegistrationEmail('no-reply@example.com', email, tokenUrl);
    }

    async confirmEmail(email : string, token : string) : Promise<boolean>
    {
        const valid = await localRA.verifyToken(email, token);
        if(valid)
        {
            await localRA.confirmEmail(email);
        }

        return valid;
    }

    async verifyToken(email : string, token : string) : Promise<boolean>
    {
        return localRA.verifyToken(email, token);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new AccountManager();

// ---------------------------------------------------------------------------------------------------------------------
