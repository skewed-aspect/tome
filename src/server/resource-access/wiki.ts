// ---------------------------------------------------------------------------------------------------------------------
// WikiResourceAccess
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

// Interfaces
import { WikiPage, WikiRev, WikiStatus } from '../../interfaces/wiki';

// Utils
import dbUtil from '../utils/database';
import { shortID } from '../utils/idGen';
import { NotFoundError } from '../errors';

// ---------------------------------------------------------------------------------------------------------------------

interface WikiPageDBRecord
{
    path : string;
    created : number;
    latest_rev : string;
}

interface WikiRevDBRecord
{
    rev_id : string;
    path : string;
    status : WikiStatus;
    title : string;
    headline : string;
    tags : string;
    contents : string;
    created : number;
    account : string;
}

// ---------------------------------------------------------------------------------------------------------------------

class WikiResourceAccess
{
    #getPageQuery(db : Knex) : Knex.QueryBuilder<WikiPageDBRecord>
    {
        return db('wiki_page')
            .select(
                'path',
                db.raw('strftime(\'%s\', created) as created'),
                'latest_rev'
            );
    }

    #getRevQuery(db : Knex) : Knex.QueryBuilder<WikiRevDBRecord>
    {
        return db('wiki_rev')
            .select(
                'rev_id',
                'path',
                'status',
                'title',
                'headline',
                'tags',
                'contents',
                db.raw('strftime(\'%s\', created) as created'),
                'account'
            );
    }

    /**
     * Checks to see if a page exists. A page is defined as existing if it has a wiki_page record and the most recent,
     * non-draft revision has the status 'published'.
     *
     * @param path - the path to the wiki page.
     *
     * @returns Returns true if the most recent non-draft version is published.
     */
    async exists(path : string) : Promise<boolean>
    {
        const db = await dbUtil.getDB('wiki');

        const [ page ] : WikiPageDBRecord[] = await this.#getPageQuery(db)
            .where({ path });

        if(page)
        {
            const revs : WikiRevDBRecord[] = await this.#getRevQuery(db)
                .where({ path })
                .orderBy('created', 'desc');

            // Get the latest revision
            const latestRev = revs.find((rev) => rev.rev_id === page.latest_rev);

            // We exist if the latest revision is published.
            return latestRev?.status === 'published';
        }

        return false;
    }

    /**
     * Gets a wiki page. Throws a `NotFoundError` if the page doesn't exist.
     *
     * @param path - the path of the wiki page.
     * @param includeDrafts - if true, get the latest revision, even if it's a draft.
     *
     * @returns Returns a `WikiPage` instance.
     */
    async get(path : string, includeDrafts = false) : Promise<WikiPage>
    {
        const db = await dbUtil.getDB('wiki');

        const [ page ] : WikiPageDBRecord[] = await this.#getPageQuery(db)
            .where({ path });

        if(page)
        {
            let [ rev ] : WikiRevDBRecord[] = await this.#getRevQuery(db)
                .where({ rev_id: page.latest_rev });

            if(includeDrafts)
            {
                const [ latestDraftRev ] = await this.#getRevQuery(db)
                    .where({ path, status: 'draft' })
                    .orderBy('created', 'desc')
                    .limit(1);

                // If the latest draft is newer than the current 'latest', we use that.
                if(latestDraftRev && latestDraftRev.created > rev.created)
                {
                    rev = latestDraftRev;
                }
            }
            else if(rev.status === 'draft')
            {
                throw new NotFoundError(`wiki page at path '${ path }'`)
            }

            if(rev)
            {
                return {
                    baseRevID: rev.rev_id,
                    path: page.path,
                    status: rev.status,
                    title: rev.title,
                    headline: rev.headline,
                    tags: JSON.parse(rev.tags),
                    contents: rev.contents,
                    created: page.created,
                    updated: rev.created,
                    account: 'unknown' // TODO: Update this to be a real account at some point.
                }
            }
        }

        throw new NotFoundError(`wiki page at path '${ path }'`);
    }

    /**
     * Gets a list of all the wiki revisions for a particular page. Throws a `NotFoundError` if no page exists.
     *
     * @param path - the path of the wiki page.
     *
     * @returns Returns a list of `WikiRevDB` items.
     */
    async getRevisions(path : string) : Promise<WikiRev[]>
    {
        const db = await dbUtil.getDB('wiki');

        const [ page ] : WikiPageDBRecord[] = await this.#getPageQuery(db)
            .where({ path });

        if(page)
        {
            const revs : WikiRevDBRecord[] = await this.#getRevQuery(db)
                .where({ path })
                .orderBy('created', 'desc');

            return revs.map((rev) =>
            {
                return {
                    revID: rev.rev_id,
                    path: rev.path,
                    status: rev.status,
                    title: rev.title,
                    headline: rev.headline,
                    tags: JSON.parse(rev.tags),
                    contents: rev.contents,
                    created: rev.created,
                    account: ''
                };
            });
        }

        throw new NotFoundError(`wiki page at path '${ path }'`);
    }

    /**
     * Adds or updates a wiki page. This always adds a new revision, and then sets the latest revision to that.
     *
     * @param path - the path of the wiki page.
     * @param page - the new page contents.
     * @param account - the account that made the edit.
     *
     * @returns Returns the new revision id that was generated.
     */
    async set(
        path : string,
        page : Omit<WikiPage, 'path' | 'created' | 'updated' | 'account'>, account : string = ''
    ) : Promise<string>
    {
        const db = await dbUtil.getDB('wiki');

        // Check for page existence.
        const exists = await this.exists(path);

        // Generate a new revision ID
        const revID = shortID();

        // Add a new revision
        await db('wiki_rev')
            .insert({
                rev_id: revID,
                path,
                status: page.status,
                title: page.title,
                headline: page.headline,
                tags: JSON.stringify(page.tags),
                contents: page.contents,
                account
            });

        // We don't set drafts as the 'latest' revision.
        if(!exists || page.status !== 'draft')
        {
            // Update the page, or add a new one.
            await db('wiki_page')
                .insert({
                    path,
                    latest_rev: revID
                })
                .onConflict('path')
                .merge();
        }

        return revID;
    }

    /**
     * Discards the current draft, if any, and all draft revisions newer than the current published revision.
     *
     * @param path - the path of the wiki page.
     *
     * @returns Always returns void.
     */
    async discardDrafts(path : string) : Promise<void>
    {
        const db = await dbUtil.getDB('wiki');
        const [ page ] : WikiPageDBRecord[] = await this.#getPageQuery(db)
            .where({ path });

        if(page)
        {
            const [ rev ] : WikiRevDBRecord[] = await this.#getRevQuery(db)
                .where({ rev_id: page.latest_rev });

            if(rev)
            {
                await db('wiki_rev')
                    .delete()
                    .where({ path, status: 'draft' })
                    .andWhere('created', '>=', rev.created)
                    .andWhere('rev_id', '<>', page.latest_rev);
            }
        }
        else
        {
            throw new NotFoundError(`wiki page at path '${ path }'`);
        }
    }

    /**
     * Deletes a wiki page. If `purge` is passed, it actually deletes the page, otherwise it adds a revision that marks
     * the page as deleted.
     *
     * @param path - the path of the wiki page.
     * @param account - the account that made the edit.
     * @param purge - if true, permanently deletes the page.
     *
     * @returns Always returns void.
     */
    async remove(path : string, account: string = '', purge = false) : Promise<void>
    {
        const db = await dbUtil.getDB('wiki');
        if(purge)
        {
            await db('wiki_page')
                .delete()
                .where({ path });

            await db('wiki_rev')
                .delete()
                .where({ path });
        }
        else
        {
            const [ page ] : WikiPageDBRecord[] = await this.#getPageQuery(db)
                .where({ path });

            if(page)
            {
                // You can only 'delete' published pages. Deleting a draft does nothing, and we shouldn't double-delete.
                const [ rev ] : WikiRevDBRecord[] = await this.#getRevQuery(db)
                    .where({ rev_id: page.latest_rev, status: 'published' });

                if(rev)
                {
                    // Generate a new revision ID
                    const revID = shortID();

                    // Add a new revision
                    await db('wiki_rev')
                        .insert({
                            rev_id: revID,
                            path,
                            status: 'deleted',
                            title: '',
                            headline: '',
                            tags: '[]',
                            contents: '',
                            account
                        });

                    // Update the page, or add a new one.
                    await db('wiki_page')
                        .insert({
                            path,
                            latest_rev: revID
                        })
                        .onConflict('path')
                        .merge();
                }
            }
        }
    }

    /**
     * Prunes a page's history, collapsing it down to just the latest version. If `discard` is true, all drafts will be
     * discarded. If `purge` is true, purges deleted pages. There will only be a single revision left for the page, or
     * it will be deleted (in the case of `purge`).
     *
     * @param path - the path of the wiki page.
     * @param discard - if true, discard any drafts
     * @param purge - if true, permanently deletes the page.
     *
     * @returns Always returns void.
     */
    async prune(path : string, discard = true, purge = true) : Promise<void>
    {
        if(discard)
        {
            await this.discardDrafts(path);
        }

        const db = await dbUtil.getDB('wiki');
        const [ latestRev ] : WikiRevDBRecord[] = await db('wiki_page as p')
            .select('r.*')
            .join('wiki_rev as r', 'r.rev_id', 'p.latest_rev')
            .where({ 'p.path': path });

        if(latestRev)
        {
            if(latestRev.status === 'deleted' && purge)
            {
                await this.remove(path, '', purge);
                return;
            }

            await db('wiki_rev')
                .delete()
                .where({ path })
                .andWhere('status', '<>', 'draft')
                .andWhere('rev_id', '<>', latestRev.rev_id);
        }
        else
        {
            throw new NotFoundError(`wiki page at path '${ path }'`);
        }
    }

    /**
     * Revert copies a past page revision and makes the copy the latest revision.
     *
     * @param path - the path of the wiki page.
     * @param revID - the revision id of the wiki revision to revert to.
     * @param account - the account that made the edit.
     *
     * @returns Returns the new revision ID.
     */
    async revert(path : string, revID : string, account : string = '') : Promise<string>
    {
        const db = await dbUtil.getDB('wiki');
        const [ oldRev ] = await this.#getRevQuery(db)
            .where({ rev_id: revID })
            .limit(1);

        if(!oldRev)
        {
            throw new NotFoundError(`revision '${ revID }' of page at path '${ path }'`);
        }

        const newPage = {
            baseRevID: revID,
            path,
            status: oldRev.status,
            title: oldRev.title,
            headline: oldRev.headline,
            tags: oldRev.tags,
            contents: oldRev.contents,
            created: oldRev.created,
            updated: oldRev.created,
            account
        };

        return this.set(path, newPage, account);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new WikiResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
