//----------------------------------------------------------------------------------------------------------------------
// EmailResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import nodemailer, { TestAccount, Transporter } from 'nodemailer';
import { logging } from '@strata-js/util-logging';

// Interfaces
import { TomeConfig } from '../../interfaces/config';

// Utils
import configMan from '../utils/config';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('emailRA');

//----------------------------------------------------------------------------------------------------------------------

class EmailResourceAccess
{
    #transport : Transporter;

    async init() : Promise<void>
    {
        const { mail } = configMan.getConfig<TomeConfig>();

        if(mail)
        {
            if(mail.testingMode)
            {
                // Generate a fake account
                const account : TestAccount = await (new Promise((resolve, reject) =>
                {
                    nodemailer.createTestAccount((err, testAccount) =>
                    {
                        if(err)
                        {
                            reject(err);
                        }
                        else
                        {
                            resolve(testAccount);
                        }
                    })
                }));

                // Set up the transport
                this.#transport = nodemailer.createTransport({
                    ...account.smtp,
                    auth: {
                        user: account.user,
                        pass: account.pass
                    }
                });

                logger.warn(`Using Fake SMTP Server. You may see generates emails at ${ account.web }. Log in with:\nusername: ${ account.user }\npassword: ${ account.pass }`);
            }
            else
            {
                // Set up the transport
                this.#transport = nodemailer.createTransport({
                    ...mail
                });
            }
        }
        else
        {
            logger.warn('No SMTP mail configured.');
        }
    }

    async sendMail(from : string, to : string, subject : string, text: string, html : string) : Promise<void>
    {
        const { mail } = configMan.getConfig<TomeConfig>();

        // Send message
        const info = await this.#transport.sendMail({ from, to, subject, text, html });

        logger.debug('Message sent:', info.messageId);

        if(mail?.testingMode)
        {
            logger.debug('Message Preview:', nodemailer.getTestMessageUrl(info));
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new EmailResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
