// ---------------------------------------------------------------------------------------------------------------------
// Settings Resource Access
// ---------------------------------------------------------------------------------------------------------------------

// Interfaces
import { SettingRecord } from '../../interfaces/settings';

// Utils
import dbUtil from '../utils/database';

// ---------------------------------------------------------------------------------------------------------------------

class SettingsResourceAccess
{
    async has(name : string) : Promise<boolean>
    {
        const db = await dbUtil.getDB('wiki');
        const [ record ] = await db('setting')
            .select('name')
            .where({ name });

        return !!record;
    }

    async get<T>(name : string, defaultVal ?: T) : Promise<T | undefined>
    {
        const db = await dbUtil.getDB('wiki');
        const [ record ] = await db('setting')
            .select('details')
            .where({ name });

        if(record)
        {
            return JSON.parse(record.details);
        }
        else
        {
            return defaultVal;
        }
    }

    async list() : Promise<SettingRecord[]>
    {
        const db = await dbUtil.getDB('wiki');
        const settings : { name: string, details : string }[] = await db('setting')
            .select()
            .orderBy('name');

        return settings.map((record) =>
        {
            return {
                name: record.name,
                details: JSON.parse(record.details)
            };
        });
    }

    async set<T>(name : string, details : T) : Promise<void>
    {
        const db = await dbUtil.getDB('wiki');
        await db('setting')
            .insert({
                name,
                details: JSON.stringify(details)
            })
            .onConflict('name')
            .merge();
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new SettingsResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
