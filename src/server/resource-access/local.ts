// ---------------------------------------------------------------------------------------------------------------------
// Local Account Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import bcrypt from 'bcrypt';
import { logging } from '@strata-js/util-logging';
import { customAlphabet } from 'nanoid';
import { alphanumeric } from 'nanoid-dictionary';

// Interfaces
import { TomeConfig } from '../../interfaces/config';
import { LocalProvider } from '../../interfaces/account';

// Resource Access
import accountRA from './account';

// Utils
import dbUtil from '../utils/database';
import configUtil from '../utils/config';

// ---------------------------------------------------------------------------------------------------------------------

const nanoID = customAlphabet(alphanumeric, 22);
const logger = logging.getLogger('localRA');

// ---------------------------------------------------------------------------------------------------------------------

class LocalAccountRA
{
    async generateResetToken(email : string) : Promise<string>
    {
        const db = await dbUtil.getDB('wiki');

        // Generate the reset token
        const token = nanoID();

        // Generate a hash of the token
        const { auth } = configUtil.getConfig<TomeConfig>();
        const hash = await bcrypt.hash(token, auth.local?.saltRounds ?? 12);

        // Add a new reset token record
        await db('reset_token')
            .insert({
                email,
                hash
            });

        return token;
    }

    async verifyToken(email : string, token : string) : Promise<boolean>
    {
        const db = await dbUtil.getDB('wiki');

        // Only find tokens from the last two hours for this email address, we won't validate anything else.
        const resetTokens = await db('reset_token')
            .select()
            .where({ email })
            .whereRaw("created <= date('now', '-2 hours')");

        // Iterate over every reset token returned, and attempt to validate it. If we find one that validates, return.
        for (const record of resetTokens)
        {
            const valid = await bcrypt.compare(token, record.hash);
            if(valid)
            {
                return true;
            }
        }

        return false;
    }

    async clearTokens(email : string, onlyExpired = false) : Promise<void>
    {
        const db = await dbUtil.getDB('wiki');
        const query = db('reset_token')
            .where({ email })
            .delete();

        if(onlyExpired)
        {
            query.whereRaw("created > date('now', '-2 hours')");
        }

        // Execute
        await query;
    }

    async setPassword(email : string, password : string) : Promise<void>
    {
        const { auth } = configUtil.getConfig<TomeConfig>();
        const hash = await bcrypt.hash(password, auth.local?.saltRounds ?? 12);

        // Set Local Provider details
        await accountRA.setProvider({
            kind: 'local',
            email,
            hash
        });
    }

    async clearPassword(email : string) : Promise<void>
    {
        const db = await dbUtil.getDB('wiki');

        // We just delete the record, because why not?
        await db('auth_local')
            .delete()
            .where({ email });
    }

    async verifyPassword(email : string, password : string) : Promise<boolean>
    {
        const localDetails = await accountRA.getProvider<LocalProvider>(email, 'local');
        if(localDetails)
        {
            const valid = await bcrypt.compare(password, localDetails.hash);
            if(valid)
            {
                return true;
            }
            else
            {
                logger.debug(`Account '${ email }' failed to validate: password incorrect.`);
                return false;
            }
        }
        else
        {
            logger.debug(`Account '${ email }' does not have a password set.`);
            return false;
        }
    }

    public async confirmEmail(email : string) : Promise<void>
    {
        const db = await dbUtil.getDB('wiki');

        // We just update the record to say the account is confirmed
        await db('auth_local')
            .update({ confirmed: true })
            .where({ email });
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new LocalAccountRA();

// ---------------------------------------------------------------------------------------------------------------------
