// ---------------------------------------------------------------------------------------------------------------------
// Account Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

// Interfaces
import { Account, AuthProvider, ProviderKinds } from '../../interfaces/account';

// Utils
import dbUtil from '../utils/database';

// ---------------------------------------------------------------------------------------------------------------------

class AccountResourceAccess
{
    #getAccountBaseQuery(db : Knex) : Knex.QueryBuilder
    {
        return db('account')
            .select(
                'email',
                'name',
                'avatar',
                'settings'
            );
    }

    async get(email : string) : Promise<Account | undefined>
    {
        const db = await dbUtil.getDB('wiki');
        const [ account ] = await this.#getAccountBaseQuery(db)
            .where({ email });

        if(account)
        {
            account.settings = JSON.parse(account.settings) || {};
        }

        return account;
    }

    async getProvider<T extends AuthProvider>(email : string, provider : ProviderKinds) : Promise<T | undefined>
    {
        const db = await dbUtil.getDB('wiki');
        const [ details ] = await db(`auth_${ provider }`)
            .select()
            .where({ email });

        if(details)
        {
            return {
                ...details,
                kind: provider,
            }
        }
    }

    async setAccount(account : Account) : Promise<void>
    {
        const accountDB = {
            ...account,
            settings: JSON.stringify(account.settings)
        };

        const db = await dbUtil.getDB('wiki');
        await db('account')
            .insert(accountDB)
            .onConflict('email')
            .merge();
    }

    async setProvider(provider : AuthProvider) : Promise<void>
    {
        const { kind, ...rest } = provider;

        const db = await dbUtil.getDB('wiki');
        await db(`auth_${ kind }`)
            .insert(rest)
            .onConflict('email')
            .merge();
    }

    async remove(email : string) : Promise<void>
    {
        const db = await dbUtil.getDB('wiki');
        await db('account')
            .delete()
            .where({ email });
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new AccountResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
