// ---------------------------------------------------------------------------------------------------------------------
// Media Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { basename, dirname } from 'node:path';
import { DirectoryOverwriteError, NotFoundError } from '../../errors';

// Interfaces
import { MediaItem, MediaMetadata } from '../../../interfaces/media';

// Utils
import dbUtil from '../../utils/database';

// ---------------------------------------------------------------------------------------------------------------------

/**
 * A resource access to handle media item manipulations. Supports storing items in the database, folder creation and
 * common manipulations. Directories are treated as 'sparse'; while explicit records can be made, writing the file
 * `/foo/bar/baz/file.txt` implies the existence of `/foo`, `/foo/bar`, and `/foo/bar/baz` as directories.
 *
 * All database code should be compatible with SQLite (default) and PostgreSQL. Any other databases that happen to work
 * are unintentional, happy coincidence.
 *
 * Since files are being stored in the database, it is recommended to keep their size small; sqlite performs worse than
 * a file system at sizes over ~100k, though no testing has been done on large files (in the megs to gigs range), so
 * performance is not guaranteed. If you need more performance than this gives out of the box, this is probably not the
 * solution for you.
 */
class MediaDBResourceAccess
{
    /**
     * Returns true if there is a record in the database for the specified path. If the optional `mime` parameter is
     * used, it will only return true if an entry exists, and it matches that mimetype. _Note:_ this does not respect
     * sparse directory structures; this is only based on exact matching of database records.
     *
     * @param path - The path to check existence for.
     * @param [mime] - An optional filter for mime type.
     *
     * @returns Returns `true` if the file exists, otherwise false.
     */
    async exists(path: string, mime ?: string): Promise<boolean>
    {
        const dir = dirname(path);
        const name = basename(path);

        const db = await dbUtil.getDB('media');
        const query = db('media_item')
            .select('mime')
            .where({ dir, name });

        if(mime)
        {
            query.where({ mime });
        }

        const [ result ] = await query
            .limit(1);

        return !!result;
    }

    /**
     * Retrieves a media item from the database. Does not support sparse directories; like `exists`, it only returns
     * actual records.
     *
     * @param path - The path of the file to retrieve.
     *
     * @returns Returns a `MediaItem` if it exists, otherwise rejects with a `NotFound` error.
     */
    async get(path : string) : Promise<MediaItem>
    {
        const dir = dirname(path);
        const name = basename(path);

        const db = await dbUtil.getDB('media');
        const [ result ] = await db('media_item')
            .select()
            .where({ dir, name })
            .orderBy('name')
            .limit(1);

        if(!result)
        {
            throw new NotFoundError(`media item at path '${path}'`);
        }

        return result;
    }

    /**
     * Saves or updates a media item. It completely replaces the existing item, if any. Throws a
     * `DirectoryOverwriteError` if attempting to save a file overtop of a directory.
     *
     * @param path - The path to the media item to save.
     * @param mime - The mime type of the media item.
     * @param [data=null] - The data of the media item.
     *
     * @returns Always returns void.
     */
    async set(path: string, mime : string, data : Buffer | null = null) : Promise<void>
    {
        const dir = dirname(path);
        const name = basename(path);

        if(mime === 'inode/directory')
        {
            // Force data to be null; directories don't support data.
            data = null;
        }
        else if(data === null)
        {
            data = Buffer.from('');
        }

        if((mime !== 'inode/directory') && await this.exists(`${ path }`, 'inode/directory'))
        {
            throw new DirectoryOverwriteError(name);
        }

        const db = await dbUtil.getDB('media');
        await db('media_item')
            .insert({
                dir,
                name,
                mime,
                data
            })
            .onConflict(['dir', 'name'])
            .merge();
    }

    /**
     * Returns a list of media items at the path specified. (Supports sparse directories.) If the path to an exact
     * match is specified, the list contains just that one `MediaItem`. If a non-existent path is specified, an empty
     * array is returned.
     *
     * If `mime` is specified, it only returns items that have that mime type.
     *
     * @param path - The path to list the contents of.
     * @param [mime] - An optional filter for mime type.
     *
     * @returns Returns a list of all `MediaItems` contained in that path, filtered by mime type (if specified).
     */
    async list(path: string, mime ?: string) : Promise<MediaMetadata[]>
    {
        const dir = dirname(path);
        const name = basename(path);
        const sparseItems : Set<string> = new Set();

        const db = await dbUtil.getDB('media');

        // First, attempt to get the exact item
        const [ item ] = await db('media_item')
            .select('dir', 'name', 'mime')
            .where({ dir, name })
            .orderBy('name')
            .limit(1);

        // Look for 'implied' directories.
        const items = await db('media_item')
            .select('dir', 'name', 'mime')
            .where('dir', 'LIKE', `${ path }%`)
            .orderBy('name');

        // Build a unique set of items from the results
        items.forEach((item) =>
        {
            const dir = item.dir.slice(path.length);
            if(dir.length !== 0)
            {
                sparseItems.add(dir.slice(1).split('/')[0]);
            }
        });

        if(item && item.mime !== 'inode/directory')
        {
            if(!mime || mime === item.mime)
            {
                return [ item ];
            }
            else
            {
                return [];
            }
        }

        const query = db('media_item')
            .select('dir', 'name', 'mime')
            .where({ dir: path });

        if(mime)
        {
            query.where({ mime });
        }

        if(!mime || mime === 'inode/directory')
        {
            const results = await query;
            return results
                .concat(
                    Array.from(sparseItems)
                        .filter((name) => !results.find((item) => item.name === name))
                        .map((name) => ({ dir: path, name, mime: 'inode/directory' }))
                )
                .sort((itemA, itemB) =>
                {
                    const nameA = itemA.name.toLowerCase();
                    const nameB = itemB.name.toLowerCase();
                    if(nameA < nameB)
                    {
                        return -1;
                    }
                    else if(nameA > nameB)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                });
        }
        else
        {
            return query
        }
    }

    /**
     * Renames a media item. Does not support moving it, only changing the name. Throws a `NotFoundError` if the item
     * does not exist.
     *
     * Does not support renaming sparse directories as that would really be moving the record that's implying the
     * sparse directory in the first place.
     *
     * @param path - The path to the item to rename.
     * @param newName - The new name for the item.
     *
     * @returns Always returns void.
     */
    async rename(path : string, newName : string) : Promise<void>
    {

        if(await this.exists(path))
        {
            const dir = dirname(path);
            const name = basename(path);

            const db = await dbUtil.getDB('media');
            await db('media_item')
                .update({ name: newName })
                .where({ dir, name });
        }
        else
        {
            throw new NotFoundError(`media item at path '${path}'`);
        }
    }

    /**
     * Moves an item from one path to another. It follows the semantics of the `mv` command: if a path ends in `/` then
     * the original name of the items is preserved, and path is taken as the new path for the item. Otherwise, the last
     * part of the path is taken as the new name for the item. (ex: `move('/foo/bar.txt', '/foo/baz') will
     * rename `bar.txt` to `baz`, but `move('/foo/bar.txt', '/foo/baz/')` will move `bar.txt` to the `/foo/baz`
     * directory.)
     *
     * @param path - The path to the file to move.
     * @param newPath - Where to move the file too.
     *
     * @returns Always returns void.
     */
    async move(path : string, newPath : string) : Promise<void>
    {
        const dir = dirname(path);
        const name = basename(path);
        const db = await dbUtil.getDB('media');

        // If the new path ends with a `/` then we're moving _to_ the path specified, not renaming it.
        if(newPath.endsWith('/'))
        {
            let cleanPath = newPath
            if(newPath !== '/')
            {
                cleanPath = newPath.slice(0, -1);
            }

            await db('media_item')
                .update({ dir: cleanPath })
                .where({ dir, name });
        }
        else
        {
            // Otherwise, we are moving and renaming at the same time.
            const newDir = dirname(newPath);
            const newName = basename(newPath);

            // Update the directory
            await db('media_item')
                .update({ dir: newDir, name: newName })
                .where({ dir, name });
        }

        // If the new path ends with a `/` then we have to join 'name' with the path
        if(newPath.endsWith('/'))
        {
            newPath += name;
        }

        // Move everything directly that was under it.
        await db('media_item')
            .update({ dir: newPath })
            .where({ dir: path });

        // Move anything that was nested under it.
        await db('media_item')
            .update({
                dir: db.raw('? || substr(dir, ?)', [ newPath, path.length + 1 ])
            })
            .where('dir', 'LIKE', `${ path }%`)
    }

    /**
     * Removes a media item. If `recursive` is passed, it removes all contents, including sparse items.
     *
     * @param path - The path to the item to remove.
     * @param [recursive=false] - If `true`, it will remove all contents of a directory. (There is no effect on a file.)
     *
     * @returns Always returns void.
     */
    async remove(path : string, recursive = false) : Promise<void>
    {
        const dir = dirname(path);
        const name = basename(path);
        const db = await dbUtil.getDB('media');

        // Just delete the media item
        await db('media_item')
            .where({ dir, name })
            .delete();

        if(recursive)
        {
            // Delete anything that has a `dir` that starts with `path`.
            await db('media_item')
                .where('dir', 'LIKE', `${ path }%`)
                .delete();
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new MediaDBResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
