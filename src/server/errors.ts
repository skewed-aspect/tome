// ---------------------------------------------------------------------------------------------------------------------
// Custom Errors
// ---------------------------------------------------------------------------------------------------------------------

export class TomeError extends Error
{
    public code?: string = 'tome-error';

    constructor(message: string, code ?: string)
    {
        super(message)

        this.code = code;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class WrappedError extends TomeError
{
    public innerError : Error;

    constructor(innerError : Error, code ?: string)
    {
        super(innerError.message, code)

        this.innerError = innerError;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class NotFoundError extends TomeError
{
    constructor(item: string)
    {
        // Whatever is padded for `item`, we capitalize the first letter, so this is a proper sentence.
        super(`${ item.charAt(0).toUpperCase() }${ item.slice(1) } was not found.`, 'not-found');
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class DirectoryOverwriteError extends TomeError
{
    constructor(item: string)
    {
        super(`Cannot overwrite directory '${ item }' with file.`, 'directory-overwrite');
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class TomeHTTPError extends TomeError
{
    public statusCode : number = 500;

    constructor(message: string, code ?: string, statusCode = 500)
    {
        super(message, code)

        this.statusCode = statusCode;
    }

    toJSON() : Record<string, unknown>
    {
        return {
            name: this.constructor.name,
            message: this.message,
            stack: this.stack,
            code: this.code,
            statusCode: this.statusCode,
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class MissingRequiredParameterError extends TomeHTTPError
{
    constructor(parameter : string)
    {
        const message = `Missing required parameter '${ parameter }'.`;
        super(message, 'missing-param-error', 422);
    }
}

export class WrappedHTTPError extends TomeHTTPError
{
    public innerError : Error;

    constructor(innerError : Error, code ?: string, statusCode = 500)
    {
        super(innerError.message, code, statusCode)

        this.innerError = innerError;
    }

    toJSON() : Record<string, unknown>
    {
        return {
            name: this.constructor.name,
            message: this.message,
            stack: this.stack,
            code: this.code,
            statusCode: this.statusCode,
            innerError: {
                name: this.innerError.constructor.name,
                message: this.innerError.message,
                stack: this.innerError.stack
            }
        }
    }

    static wrapError(error : Error) : TomeHTTPError
    {
        if(!(error instanceof TomeHTTPError))
        {
            return new WrappedHTTPError(error, 'wrapped-error', 500);
        }
        else
        {
            return error;
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
