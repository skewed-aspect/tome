// ---------------------------------------------------------------------------------------------------------------------
// Request Logging Middleware
// ---------------------------------------------------------------------------------------------------------------------

import { Handler } from 'express';
import { BasicLogger } from '@strata-js/util-logging';

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Basic request logging
 *
 * @param logger - The logger to use.
 *
 * @returns Returns a middleware function to perform logging.
 */
export function requestLogger(logger ?: BasicLogger) : Handler
{
    return (request, _response, next) =>
    {
        if(logger)
        {
            logger.debug(`${ request.method } '${ request.url }'`);
        }

        next();
    };
}


// ---------------------------------------------------------------------------------------------------------------------
