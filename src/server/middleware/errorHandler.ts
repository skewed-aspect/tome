// ---------------------------------------------------------------------------------------------------------------------
// Error Handling Middleware
// ---------------------------------------------------------------------------------------------------------------------

import { BasicLogger } from '@strata-js/util-logging';
import { NextFunction, Request, Response, ErrorRequestHandler } from 'express';

import { TomeHTTPError, WrappedHTTPError } from '../errors';

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Build a custom error logger
 *
 * @param logger - The logger to use.
 *
 * @returns Returns a middleware function to perform logging.
 */
export function errorHandler(logger ?: BasicLogger) : ErrorRequestHandler
{
    // If we don't have 4 parameters, this function literally doesn't work.
    // eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
    return (err : TomeHTTPError | Error, request : Request, response : Response, _next : NextFunction) =>
    {
        const error = WrappedHTTPError.wrapError(err);

        response.status(error.statusCode);

        if(logger)
        {
            //TODO: Expand once strata logger supports child loggers.
            const childLogger = logger;
            // const childLogger = logger.child({
            //     request: {
            //         method: request.method,
            //         url: request.url,
            //         body: request.body,
            //         query: request.query
            //     }
            // });

            if(response.statusCode < 500)
            {
                childLogger.warn(`${ request.method } ${ response.statusCode } '${ request.url }': ${ error.stack }`);
            }
            else
            {
                childLogger.error(`${ request.method } ${ response.statusCode } '${ request.url }': ${ error.stack }`);
            }
        }

        response.send(error.toJSON());
    };
}

// ---------------------------------------------------------------------------------------------------------------------
