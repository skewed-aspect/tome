//----------------------------------------------------------------------------------------------------------------------
// Configuration Loader
//----------------------------------------------------------------------------------------------------------------------

import fs from 'node:fs';
import path from 'node:path';

import JSON5 from 'json5';
import envsubst from '@tuplo/envsubst';

//----------------------------------------------------------------------------------------------------------------------

class ConfigUtil
{
    #configRecords = new Map();

    #parseJSON5(contents : string) : Record<string, unknown>
    {
        return JSON5.parse(contents);
    }

    #parseJSON(content : string) : Record<string, unknown>
    {
        return JSON.parse(content);
    }

    loadConfigFile(filePath ?: string, name = 'default', envSub = true) : void
    {
        if(!filePath)
        {
            filePath = process.env.CONFIG_FILE
                ? path.isAbsolute(process.env.CONFIG_FILE)
                    ? process.env.CONFIG_FILE : path.join(process.cwd(), process.env.CONFIG_FILE)
                : path.join(process.cwd(), './config/local.json5');
        }
        const fileExt = path.extname(filePath).toLowerCase();
        const fileBuff = fs.readFileSync(filePath);
        let fileContents = fileBuff.toString();
        if(envSub)
        {
            // FIXME: This cast shouldn't be needed, but TS is being super weird.
            fileContents = (envsubst as any)(fileContents);
        }
        switch (fileExt)
        {
            case '.json5':
                this.#configRecords.set(name, this.#parseJSON5(fileContents));
                break;
            case '.json':
                this.#configRecords.set(name, this.#parseJSON(fileContents));
                break;
            default:
                throw new Error(`Cannot parse ${ fileExt } files.`);
        }
    }

    getConfig<T>(name = 'default') : T
    {
        if(!this.#configRecords.has(name))
        {
            throw new Error(`No config named ${ name } has been loaded.`);
        }
        return this.#configRecords.get(name) as T;
    }

    setConfig<T>(config : T, name = 'default') : void
    {
        this.#configRecords.set(name, config);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ConfigUtil();

//----------------------------------------------------------------------------------------------------------------------
