//----------------------------------------------------------------------------------------------------------------------
// Utilities to make express routes use less boilerplate.
//----------------------------------------------------------------------------------------------------------------------

import fs from 'fs';
import path from 'path';
import { NextFunction, Request, Response, Handler } from 'express';

//----------------------------------------------------------------------------------------------------------------------

/**
 * Serves index page.
 *
 * @param clientRoot - The directory that the client files are served from.
 */
export function serveIndex(clientRoot : string) : Handler
{
    return function indexHandler(req : Request, response: Response) : void
    {
        response.setHeader('Content-Type', 'text/html');
        fs.createReadStream(path.resolve(clientRoot, 'index.html')).pipe(response);
    }
}

/**
 * Either serve 'index.html', or run json handler
 *
 * @param response - Express response.
 * @param jsonHandler - Handler function for the json portion of the request.
 * @param clientRoot - The directory that the client files are served from.
 */
export function interceptHTML(response : Response, jsonHandler : Handler, clientRoot : string) : void
{
    response.format({
        html: serveIndex(clientRoot),
        json(req : Request, resp : Response, next : NextFunction)
        {
            try
            {
                Promise.resolve(jsonHandler(req, resp, next)).catch(next);
            }
            catch(err)
            {
                next(err);
            }
        }
    });
}

/**
 * Ensures that the user is authenticated, or it returns a 401.
 *
 * @param request - Express request.
 * @param response - Express response.
 * @param next - Express next function.
 */
export function ensureAuthenticated(request : Request, response : Response, next : NextFunction) : void
{
    if(request.isAuthenticated())
    {
        next();
    }
    else
    {
        response.status(401).json({
            name: 'NotAuthorized',
            message: `Not authorized.`
        });
    }
}

//----------------------------------------------------------------------------------------------------------------------
