// ---------------------------------------------------------------------------------------------------------------------
// Database Utility
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import { Knex, knex } from 'knex';

import ConfigUtil from './config';
import type { TomeConfig } from '../../interfaces/config';

// ---------------------------------------------------------------------------------------------------------------------

class DatabaseUtil
{
    #dbConnections : Map<string, Knex> = new Map();

    /**
     * Return a modified version of the config, that makes the sqlite file relative to the `db` folder in the root of
     * the project.
     *
     * @param db - The database config to get.
     */
    async getDBConfig(db : 'media' | 'wiki') : Promise<Knex.Config>
    {
        const { databases } = ConfigUtil.getConfig<TomeConfig>();

        const config = databases[db];
        if(config.client === 'sqlite')
        {
            // Just return the config, we can't modify it if it's a string.
            if(typeof config.connection === 'string')
            {
                return config;
            }

            // If it's a function, we have to resolve it.
            if(typeof config.connection === 'function')
            {
                config.connection = await config.connection();
            }

            // Now we know we're dealing with a Sqlite3 connection config.
            const connection : Knex.Sqlite3ConnectionConfig = config.connection as Knex.Sqlite3ConnectionConfig;

            // Return a modified config
            return {
                ...config,
                client: 'better-sqlite3',
                connection: {
                    filename: resolve(__dirname, '..', '..', '..', '..', 'db', connection?.filename ?? '')
                }
            };
        }

        return config;
    }

    async getMigrationConfig(db : 'media' | 'wiki') : Promise<Knex.MigratorConfig>
    {
        return {
            directory: resolve(__dirname, '..', '..', 'knex', 'migrations', db),
            extension: 'ts',
            stub: resolve(__dirname, '..', '..', 'knex', 'migrations', 'ts.stub'),
            loadExtensions: [ '.js' ]
        };
    }

    async getSeedConfig(db : 'media' | 'wiki') : Promise<Knex.SeederConfig>
    {
        return {
            directory: resolve(__dirname, '..', '..', 'knex', 'seeds', db),
            extension: 'ts',
            loadExtensions: [ '.js' ]
        };
    }

    async getDB(db : 'media' | 'wiki') : Promise<Knex>
    {
        let conn = this.#dbConnections.get(db);
        if(!conn)
        {
            conn = knex(await this.getDBConfig(db));
            this.#dbConnections.set(db, conn);
        }

        return conn;
    }

    async initDB(db : 'media' | 'wiki', unitTest = false) : Promise<void>
    {
        // Get the db
        const conn = await this.getDB(db);

        // Get the config
        const conf = await this.getMigrationConfig(db);

        if(unitTest)
        {
            // If we're running as a unit test, our extension is `.ts`
            conf.loadExtensions = [ '.ts' ];
        }

        // If we're connecting to the database for the first time, go ahead and run the migrations and seeds.
        await conn.migrate.latest(conf);

        // TODO: We don't have any need for seeds, right now.
        // await conn.seed.run(await this.getSeedConfig(db));
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new DatabaseUtil();

// ---------------------------------------------------------------------------------------------------------------------
