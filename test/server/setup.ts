// ---------------------------------------------------------------------------------------------------------------------
// Setup the Server Tests
// ---------------------------------------------------------------------------------------------------------------------

import { init } from '../../src/server/init';

// Configuration
import { TomeConfig } from '../../src/interfaces/config';
import configUtil from '../../src/server/utils/config';

// ---------------------------------------------------------------------------------------------------------------------

// Get configuration
const config =  configUtil.getConfig<TomeConfig>();

// Unit tests need an in-memory database, but knex times it out without some coaxing.
config.databases = {
    media: {
        client: 'better-sqlite3',
        connection: ':memory:',
        useNullAsDefault: true,
        pool: {
            min: 1,
            max: 1,
            idleTimeoutMillis: 360000 * 1000,
            destroyTimeoutMillis: 360000 * 1000
        }
    },
    wiki: {
        client: 'better-sqlite3',
        connection: ':memory:',
        useNullAsDefault: true,
        pool: {
            min: 1,
            max: 1,
            idleTimeoutMillis: 360000 * 1000,
            destroyTimeoutMillis: 360000 * 1000
        }
    }
}

// Make sure dev mode is off.
config.server.devMode = false;

// Set logging
config.logging = {
    level: 'error',
    prettyPrint: true
}

// Set the new configuration
configUtil.setConfig(config);

// ---------------------------------------------------------------------------------------------------------------------

init(true)
    .then(() =>
    {
        // This is added to the global context by running mocha with `--delay`.
        // See: https://mochajs.org/#delayed-root-suite
        // eslint-disable-next-line no-undef
        // @ts-ignore
        run();
    });

// ---------------------------------------------------------------------------------------------------------------------
