// ---------------------------------------------------------------------------------------------------------------------
// Setting Resource Access Tests
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';
import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';

// Utils
import dbUtil from '../../../src/server/utils/database';

// Resource Access
import settingsRA from '../../../src/server/resource-access/settings';

// ---------------------------------------------------------------------------------------------------------------------

chai.use(chaiAsPromised);

// ---------------------------------------------------------------------------------------------------------------------

describe('Settings Resource Access', () =>
{
    let db : Knex;

    beforeEach(async () =>
    {
        db = await dbUtil.getDB('wiki');

        // Load the test cases
        await db('setting')
            .insert([
                { name: 'test', details: '{"setting1":"foo","setting2":true,"nested":{"foo":"bar"}}' },
                { name: 'num', details: '420' },
                { name: 'bool', details: 'true' },
                { name: 'string', details: '"test"' },
                { name: 'arrayTest', details: '["item1",720,true]' },
                { name: 'discord', details: '{"clientID":"1234","clientSecret":"98765432a"}' },
            ]);
    });

    afterEach(async () =>
    {
        // Clear the database
        await db('setting').truncate();
    });

    describe('get', () =>
    {
        it('retrieves setting records by key name', async () =>
        {
            const discord = await settingsRA.get<{ clientID : string, clientSecret : string }>('discord');
            expect(discord).to.have.property('clientID', '1234');
            expect(discord).to.have.property('clientSecret', '98765432a');
        });

        it('supports default values if the record is not found', async () =>
        {
            const defaultVal = await settingsRA.get<{ foo: boolean }>('dne', { foo: true });
            expect(defaultVal).to.have.property('foo', true);
        });

        it('supports retrieving objects', async () =>
        {
            const test = await settingsRA.get('test');
            expect(test).to.have.property('setting1', 'foo');
            expect(test).to.have.property('setting2', true);
            expect(test).to.have.nested.property('nested.foo', 'bar');
        });

        it('supports retrieving arrays', async () =>
        {
            const array = await settingsRA.get('arrayTest');
            expect(array).to.be.an('array');
            expect(array).to.have.length(3);

            // Make typescript shut up.
            if(array && array instanceof Array)
            {
                expect(array[0]).to.equal('item1');
                expect(array[1]).to.equal(720);
                expect(array[2]).to.equal(true);
            }
        });

        it('supports retrieving literals', async () =>
        {
            const num = await settingsRA.get('num');
            expect(num).to.equal(420);

            const bool = await settingsRA.get('bool');
            expect(bool).to.equal(true);

            const string = await settingsRA.get('string');
            expect(string).to.equal('test');
        });
    });

    describe('list', () =>
    {
        it('lists all current setting records', async () =>
        {
            const settings = await settingsRA.list();
            expect(settings).to.be.an('array');
            expect(settings).to.have.length(6);

            // Check the first one
            expect(settings[0].details).to.be.an('array');
            expect(settings[0].details).to.have.length(3);
            expect(settings[0].details[0]).to.equal('item1');
        });
    });

    describe('set', () =>
    {
        it('allows setting new setting record', async () =>
        {
            const before = await settingsRA.get('new');
            expect(before).to.be.undefined;

            // Set a new setting
            await settingsRA.set('new', { foo: 'bar', baz: true });

            const after = await settingsRA.get('new');
            expect(after).to.have.property('foo', 'bar');
            expect(after).to.have.property('baz', true);
        });

        it('allows updating an existing record', async () =>
        {
            const discord = await settingsRA.get<{ clientID : string, clientSecret : string }>('discord');
            expect(discord).to.have.property('clientID', '1234');
            expect(discord).to.have.property('clientSecret', '98765432a');

            // Set an existing record
            await settingsRA.set('discord', { clientID: '5678', clientSecret: '12345678a' });

            const discordAfter = await settingsRA.get<{ clientID : string, clientSecret : string }>('discord');
            expect(discordAfter).to.have.property('clientID', '5678');
            expect(discordAfter).to.have.property('clientSecret', '12345678a');
        });

        it('supports setting an object', async () =>
        {
            const before = await settingsRA.get('new');
            expect(before).to.be.undefined;

            // Set a new setting
            await settingsRA.set('new', { foo: 'bar', baz: true });

            const after = await settingsRA.get('new');
            expect(after).to.have.property('foo', 'bar');
            expect(after).to.have.property('baz', true);
        });

        it('supports setting an array', async () =>
        {
            const before = await settingsRA.get('new');
            expect(before).to.be.undefined;

            // Set a new setting
            await settingsRA.set('new', [ 'bar', true ]);

            const after = await settingsRA.get('new');
            expect(after).to.be.an('array');
            expect(after).to.have.length(2);

            // Make typescript shut up.
            if(after && after instanceof Array)
            {
                expect(after[0]).to.equal('bar');
                expect(after[1]).to.equal(true);
            }
        });

        it('supports setting a literal', async () =>
        {
            const num = await settingsRA.get('num');
            expect(num).to.equal(420);

            await settingsRA.set('num', 69);

            const numAfter = await settingsRA.get('num');
            expect(numAfter).to.equal(69);

            const bool = await settingsRA.get('bool');
            expect(bool).to.equal(true);

            await settingsRA.set('bool', false);

            const boolAfter = await settingsRA.get('bool');
            expect(boolAfter).to.equal(false);

            const string = await settingsRA.get('string');
            expect(string).to.equal('test');

            await settingsRA.set('string', 'test');

            const stringAfter = await settingsRA.get('string');
            expect(stringAfter).to.equal('test');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
