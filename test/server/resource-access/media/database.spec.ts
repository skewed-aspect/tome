// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the MediaDBResourceAccess module.
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';
import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';

// Utils
import dbUtil from '../../../../src/server/utils/database';

// Resource Access

import mediaRA from '../../../../src/server/resource-access/media/database';
import { DirectoryOverwriteError, NotFoundError } from '../../../../src/server/errors';

// ---------------------------------------------------------------------------------------------------------------------

chai.use(chaiAsPromised);

// ---------------------------------------------------------------------------------------------------------------------

describe('Media Resource Access', () =>
{
    let db : Knex;

    beforeEach(async () =>
    {
        db = await dbUtil.getDB('media');

        // Load the test cases
        await db('media_item')
            .insert([
                { dir: '/', name: 'foo', mime: 'inode/directory', data: null },
                { dir: '/', name: 'emptyDir', mime: 'inode/directory', data: null },
                { dir: '/', name: 'bar.txt', mime: 'text/plain', data: Buffer.from('test') },
                { dir: '/foo', name: 'nested', mime: 'inode/directory', data: null },
                { dir: '/foo/nested', name: 'nested2', mime: 'inode/directory', data: null },
                { dir: '/foo/nested', name: 'test1.txt', mime: 'text/plain', data: Buffer.from('test') },
                { dir: '/foo/nested', name: 'test2.txt', mime: 'text/plain', data: Buffer.from('test') },
                { dir: '/foo/nested', name: 'test.json', mime: 'application/json', data: Buffer.from('{"name":"test"}') },
                { dir: '/foo/nested/nested2', name: 'deep.txt', mime: 'text/plain', data: Buffer.from('test') },
                { dir: '/foo/bar', name: 'sparse.txt', mime: 'text/plain', data: Buffer.from('test') },
                { dir: '/foo/bar/baz', name: 'nested2.txt', mime: 'text/plain', data: Buffer.from('test') },
                { dir: '/foo/bar/baz', name: 'nestedDir', mime: 'inode/directory', data: null },
                { dir: '/foo/bar/other', name: 'file.txt', mime: 'text/plain', data: Buffer.from('test') },
            ]);
    });

    afterEach(async () =>
    {
        // Clear the database
        await db('media_item').truncate();
    });

    describe('exists', () =>
    {
        it('returns true when passed an existing file path', async () =>
        {
            const exists = await mediaRA.exists('/bar.txt');
            expect(exists).to.be.equal(true);

            const nested = await mediaRA.exists('/foo/nested/test1.txt');
            expect(nested).to.be.equal(true);
        });

        it('returns true when passed an existing directory path', async () =>
        {
            const exists = await mediaRA.exists('/foo');
            expect(exists).to.be.equal(true);

            const nested = await mediaRA.exists('/foo/nested');
            expect(nested).to.be.equal(true);
        });

        it('returns false when passed a non-existent file path', async () =>
        {
            const exists = await mediaRA.exists('/does-not-exist.txt');
            expect(exists).to.be.equal(false);

            const nested = await mediaRA.exists('/dne/dne/dne/dne/dne/foo.txt');
            expect(nested).to.be.equal(false);
        });

        it('returns false when passed a non-existent directory path', async () =>
        {
            const exists = await mediaRA.exists('/does-not-exist');
            expect(exists).to.be.equal(false);

            const nested = await mediaRA.exists('/dne/dne/dne/dne/dne/foo');
            expect(nested).to.be.equal(false);
        });

        describe('Sparse item handling', () =>
        {
            it('returns true on sparse items with implied directories', async () =>
            {
                const exists = await mediaRA.exists('/foo/bar/baz/nested2.txt');
                expect(exists).to.be.equal(true);

                const exists2 = await mediaRA.exists('/foo/bar/baz/nestedDir');
                expect(exists2).to.be.equal(true);
            });

            it('returns false for implied directories on sparse items', async () =>
            {
                const exists = await mediaRA.exists('/foo/bar');
                expect(exists).to.be.equal(false);

                const exists2 = await mediaRA.exists('/foo/bar/dne.txt');
                expect(exists2).to.be.equal(false);
            });
        });

        describe('Optional mime filtering', () =>
        {
            it('returns true for existing items with matching mime types', async () =>
            {
                let exists = await mediaRA.exists('/foo', 'inode/directory');
                expect(exists).to.be.equal(true);

                let nested = await mediaRA.exists('/foo/nested', 'inode/directory');
                expect(nested).to.be.equal(true);

                exists = await mediaRA.exists('/bar.txt', 'text/plain');
                expect(exists).to.be.equal(true);

                nested = await mediaRA.exists('/foo/nested/test1.txt', 'text/plain');
                expect(nested).to.be.equal(true);
            });

            it('returns false for existing items with non-matching mime types', async () =>
            {
                let exists = await mediaRA.exists('/foo', 'text/dne');
                expect(exists).to.be.equal(false);

                let nested = await mediaRA.exists('/foo/nested', 'text/text');
                expect(nested).to.be.equal(false);

                exists = await mediaRA.exists('/bar.txt', 'inode/directory');
                expect(exists).to.be.equal(false);

                nested = await mediaRA.exists('/foo/nested/test1.txt', 'inode/directory');
                expect(nested).to.be.equal(false);
            });
        });
    });

    describe('get', () =>
    {
        it('returns an item if the item exists', async () =>
        {
            const file = await mediaRA.get('/bar.txt');
            expect(file).to.have.property('name', 'bar.txt');
            expect(file).to.have.property('mime', 'text/plain');
        });

        it('returns an item with null data if path is a directory', async () =>
        {
            const file = await mediaRA.get('/foo');
            expect(file).to.have.property('name', 'foo');
            expect(file).to.have.property('mime', 'inode/directory');
            expect(file).to.have.property('data', null);
        });

        it('returns an object with a buffer for data if path is a file', async () =>
        {
            const file = await mediaRA.get('/bar.txt');
            expect(file).to.have.property('data');
            expect(file.data).to.be.a.instanceOf(Buffer);
            expect((file.data as Buffer).toString()).to.equal('test');
        });

        it('rejects with a NotFoundError if no item exists', async () =>
        {
            await expect(mediaRA.get('/dne.txt')).to.be.rejectedWith(NotFoundError);
        });

        describe('Sparse item handling', () =>
        {
            it('rejects with a NotFoundError if called on an implied directory from a sparse item', async () =>
            {
                await expect(mediaRA.get('/foo/bar')).to.be.rejectedWith(NotFoundError);
            });
        });
    });

    describe('set', () =>
    {
        it('allows inserting of new files', async () =>
        {
            // Insert new file
            await mediaRA.set('/new.txt', 'text/plain', Buffer.from('new file who dis'));

            // Check to make sure it inserted
            const file = await mediaRA.get('/new.txt');
            expect(file).to.have.property('name', 'new.txt');
            expect(file).to.have.property('dir', '/');
            expect(file).to.have.property('mime', 'text/plain');
            expect(file.data).to.be.a.instanceOf(Buffer);
            expect((file.data as Buffer).toString()).to.equal('new file who dis');
        });

        it('inserts an empty buffer if no data is passed when creating a new file', async () =>
        {
            // Insert new file
            await mediaRA.set('/new.txt', 'text/plain');

            // Check to make sure it inserted
            const file = await mediaRA.get('/new.txt');
            expect(file).to.have.property('name', 'new.txt');
            expect(file).to.have.property('dir', '/');
            expect(file).to.have.property('mime', 'text/plain');
            expect(file.data).to.be.a.instanceOf(Buffer);
            expect((file.data as Buffer).toString()).to.equal('');
        });

        it('allows inserting of new directories', async () =>
        {
            // Insert new file
            await mediaRA.set('/newFolder', 'inode/directory');

            // Check to make sure it inserted
            const file = await mediaRA.get('/newFolder');
            expect(file).to.have.property('name', 'newFolder');
            expect(file).to.have.property('dir', '/');
            expect(file).to.have.property('mime', 'inode/directory');
            expect(file).to.have.property('data', null);
        });

        it('does not allow inserting directories with data', async () =>
        {
            // Insert new file
            await mediaRA.set('/newFolder', 'inode/directory', Buffer.from('oops'));

            // Check to make sure it inserted
            const file = await mediaRA.get('/newFolder');
            expect(file).to.have.property('name', 'newFolder');
            expect(file).to.have.property('dir', '/');
            expect(file).to.have.property('mime', 'inode/directory');
            expect(file).to.have.property('data', null);
        });

        it('allows updating of existing items', async () =>
        {
            const file = await mediaRA.get('/bar.txt');
            expect(file).to.have.property('name', 'bar.txt');
            expect(file).to.have.property('mime', 'text/plain');

            // Update the item.
            await mediaRA.set('/bar.txt', 'application/json', Buffer.from('new content'));

            // Check that the update went through
            const file2 = await mediaRA.get('/bar.txt');
            expect(file2).to.have.property('mime', 'application/json');
            expect((file2.data as Buffer).toString()).to.equal('new content');
        });

        it('rejects with an error if saving a file overtop of a directory', async () =>
        {
            // Insert new file
            await mediaRA.set('/testThing', 'inode/directory');

            // Update it to a directory
            await expect(mediaRA.set('/testThing', 'text/plain', Buffer.from('not a dir')))
                .to.be.rejectedWith(DirectoryOverwriteError);
        });

        describe('Sparse item handling', () =>
        {
            it('allows creation of real records over top of implied directories in sparse items', async () =>
            {
                // Insert new file
                await mediaRA.set('/foo/bar', 'inode/directory');

                // Check to make sure it inserted
                const file = await mediaRA.get('/foo/bar');
                expect(file).to.have.property('name', 'bar');
                expect(file).to.have.property('dir', '/foo');
                expect(file).to.have.property('mime', 'inode/directory');
                expect(file).to.have.property('data', null);
            });
        });
    });

    describe('list', () =>
    {
        it('returns an array of one item if path is a file', async () =>
        {
            const files = await mediaRA.list('/bar.txt');
            expect(files).to.be.an.instanceOf(Array);
            expect(files).to.have.length(1);
            expect(files[0]).to.have.property('name', 'bar.txt');
            expect(files[0]).to.have.property('mime', 'text/plain');
        });

        it('returns an array of all items contained inside if path is a directory', async () =>
        {
            const files = await mediaRA.list('/foo/nested');
            expect(files).to.be.an.instanceOf(Array);
            expect(files).to.have.length(4);
            expect(files[0]).to.have.property('name', 'nested2');
            expect(files[0]).to.have.property('mime', 'inode/directory');
            expect(files[1]).to.have.property('name', 'test.json');
            expect(files[2]).to.have.property('name', 'test1.txt');
            expect(files[3]).to.have.property('name', 'test2.txt');
        });

        it('returns an empty array when passed a non-existent file path', async () =>
        {
            const files = await mediaRA.list('/dne.txt');
            expect(files).to.be.an.instanceOf(Array);
            expect(files).to.have.length(0);
        });

        describe('Sparse item handling', () =>
        {
            it('returns implied directories from sparse items when listing a directory', async () =>
            {
                const files = await mediaRA.list('/foo/bar');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(3);
                expect(files[0]).to.have.property('name', 'baz');
                expect(files[1]).to.have.property('name', 'other');
                expect(files[2]).to.have.property('name', 'sparse.txt');

                const files2 = await mediaRA.list('/foo/bar/baz');
                expect(files2).to.be.an.instanceOf(Array);
                expect(files2).to.have.length(2);
                expect(files2[0]).to.have.property('name', 'nested2.txt');
                expect(files2[1]).to.have.property('name', 'nestedDir');
            });

            it('correctly handles a mixture of real and sparse contents when listing a directory', async () =>
            {
                // Ensure the directory is there
                const files = await mediaRA.list('/foo');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(2);
                expect(files[0]).to.have.property('name', 'bar');
                expect(files[1]).to.have.property('name', 'nested');
            });
        });

        describe('Optional mime filtering', () =>
        {
            it('returns and array of items matching the mimetype contained inside if path is a directory', async () =>
            {
                const files = await mediaRA.list('/foo/nested', 'text/plain');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(2);
                expect(files[0]).to.have.property('name', 'test1.txt');
                expect(files[1]).to.have.property('name', 'test2.txt');
            });

            it('returns an array of one item if path is a file and that type matched the mime type', async () =>
            {
                const files = await mediaRA.list('/bar.txt', 'text/plain');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(1);
                expect(files[0]).to.have.property('name', 'bar.txt');
                expect(files[0]).to.have.property('mime', 'text/plain');
            });

            it('returns a empty array if path is a file and it does not match the mime type', async () =>
            {
                const files = await mediaRA.list('/bar.txt', 'fake/mime-type');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(0);
            });
        });
    });

    describe('rename', () =>
    {
        it('renames an existing file', async () =>
        {
            await mediaRA.rename('/bar.txt', 'baz.txt');

            const file = await mediaRA.get('/baz.txt');
            expect(file).to.have.property('name', 'baz.txt');
            expect(file).to.have.property('mime', 'text/plain');
            expect((file.data as Buffer).toString()).to.equal('test');
        });

        it('renames an existing folder', async () =>
        {
            await mediaRA.rename('/emptyDir', 'omgbbq');

            const dir = await mediaRA.get('/omgbbq');
            expect(dir).to.have.property('name', 'omgbbq');
            expect(dir).to.have.property('mime', 'inode/directory');
        });

        it('rejects with an error if the item does not exist', async () =>
        {
            await expect(mediaRA.rename('/does-not-exist', 'still-nope')).to.be.rejectedWith(NotFoundError);
        });

        describe('Sparse item handling', () =>
        {
            it('rejects with an error if attempting to rename an implied directory from a sparse item', async () =>
            {
                await expect(mediaRA.rename('/foo/bar', 'sparse-test')).to.be.rejectedWith(NotFoundError);
            });
        });
    });

    describe('move', () =>
    {
        it('moves and renames an existing file', async () =>
        {
            await mediaRA.move('/bar.txt', '/foo/test.txt');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/bar.txt');
            expect(oldExists).to.equal(false);

            // Ensure the file is in the correct location
            const file = await mediaRA.get('/foo/test.txt');
            expect(file).to.have.property('name', 'test.txt');
            expect(file).to.have.property('mime', 'text/plain');
            expect((file.data as Buffer).toString()).to.equal('test');
        });

        it('moves an existing file to the specified directory if it ends in a /', async () =>
        {
            await mediaRA.move('/bar.txt', '/foo/');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/bar.txt');
            expect(oldExists).to.equal(false);

            // Ensure the file is in the correct location
            const file = await mediaRA.get('/foo/bar.txt');
            expect(file).to.have.property('name', 'bar.txt');
            expect(file).to.have.property('mime', 'text/plain');
            expect((file.data as Buffer).toString()).to.equal('test');

            await mediaRA.move('/foo/bar.txt', '/');

            // Ensure the file no longer exists at its current path
            const oldExists2 = await mediaRA.exists('/foo/bar.txt');
            expect(oldExists2).to.equal(false);

            // Ensure the file is in the correct location
            const file2 = await mediaRA.get('/bar.txt');
            expect(file2).to.have.property('name', 'bar.txt');
            expect(file2).to.have.property('mime', 'text/plain');
            expect((file2.data as Buffer).toString()).to.equal('test');
        });

        it('moves and renames an existing directory', async () =>
        {
            await mediaRA.move('/emptyDir', '/foo/empty');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/emptyDir');
            expect(oldExists).to.equal(false);

            // Ensure the file is in the correct location
            const file = await mediaRA.get('/foo/empty');
            expect(file).to.have.property('name', 'empty');
            expect(file).to.have.property('mime', 'inode/directory');
        });

        it('moves an existing directory to the specified directory if it ends in a /', async () =>
        {
            await mediaRA.move('/emptyDir', '/foo/');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/emptyDir');
            expect(oldExists).to.equal(false);

            // Ensure the file is in the correct location
            const file = await mediaRA.get('/foo/emptyDir');
            expect(file).to.have.property('name', 'emptyDir');
            expect(file).to.have.property('mime', 'inode/directory');
        });

        it('moves the contents of a directory when that directory is moved', async () =>
        {
            await mediaRA.move('/foo/nested', '/moved/');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/foo/nested');
            expect(oldExists).to.equal(false);

            const files = await mediaRA.list('/moved/nested');
            expect(files).to.be.an.instanceOf(Array);
            expect(files).to.have.length(4);
            expect(files[0]).to.have.property('name', 'nested2');
            expect(files[0]).to.have.property('mime', 'inode/directory');
            expect(files[1]).to.have.property('name', 'test.json');
            expect(files[2]).to.have.property('name', 'test1.txt');
            expect(files[3]).to.have.property('name', 'test2.txt');
        });

        it('correctly handles deeply nested items when a directory is moved', async () =>
        {
            await mediaRA.move('/foo/nested', '/moved/');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/foo/nested');
            expect(oldExists).to.equal(false);

            const files = await mediaRA.list('/moved/nested/nested2');
            expect(files).to.be.an.instanceOf(Array);
            expect(files).to.have.length(1);
            expect(files[0]).to.have.property('name', 'deep.txt');
        });

        describe('Sparse item handling', () =>
        {
            it('correctly handles sparse items when moving a directory with an actual record', async () =>
            {
                // Ensure the directory is there
                const files = await mediaRA.list('/foo');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(2);

                await mediaRA.move('/foo', '/moved');

                // Ensure the directory moved
                const filesAfter = await mediaRA.list('/moved');
                expect(filesAfter).to.be.an.instanceOf(Array);
                expect(filesAfter).to.have.length(2);

                // Check individual files.
                await expect(mediaRA.exists('/moved/nested/nested2')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/nested/test1.txt')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/nested/test2.txt')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/nested/test.json')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/bar/sparse.txt')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/bar/baz/nested2.txt')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/bar/baz/nestedDir')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/bar/other/file.txt')).to.eventually.equal(true);
            });

            it('correctly handles sparse items when moving an implied directory', async () =>
            {
                // Ensure the directory is there
                const files = await mediaRA.list('/foo/bar');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(3);

                await mediaRA.move('/foo/bar', '/moved');

                // Ensure the directory moved
                const filesAfter = await mediaRA.list('/moved');
                expect(filesAfter).to.be.an.instanceOf(Array);
                expect(filesAfter).to.have.length(3);

                // Check individual files.
                await expect(mediaRA.exists('/moved/sparse.txt')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/baz/nested2.txt')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/baz/nestedDir')).to.eventually.equal(true);
                await expect(mediaRA.exists('/moved/other/file.txt')).to.eventually.equal(true);
            });
        });
    });

    describe('remove', () =>
    {
        it('removes an existing file', async () =>
        {
            // Ensure the file currently exists
            const exists = await mediaRA.exists('/bar.txt');
            expect(exists).to.equal(true);

            // Remove the file
            await mediaRA.remove('/bar.txt');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/bar.txt');
            expect(oldExists).to.equal(false);
        });

        it('removes an existing directory', async () =>
        {
            // Ensure the file currently exists
            const exists = await mediaRA.exists('/emptyDir');
            expect(exists).to.equal(true);

            // Remove the file
            await mediaRA.remove('/emptyDir');

            // Ensure the file no longer exists at its current path
            const oldExists = await mediaRA.exists('/emptyDir');
            expect(oldExists).to.equal(false);
        });

        it('does not throw on non-existent files or directories', async () =>
        {
            // Ensure the file currently does not exist
            const exists = await mediaRA.exists('/dne');
            expect(exists).to.equal(false);

            // Remove the file
            await mediaRA.remove('/dne');
        });

        describe('Sparse item handling', () =>
        {
            it('correctly removes sparse items if recursive is passed', async () =>
            {
                // Ensure the directory is there
                const files = await mediaRA.list('/foo/bar');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(3);

                // Ensure an actual record is there
                const exists = await mediaRA.exists('/foo/bar/sparse.txt');
                expect(exists).to.equal(true);

                await mediaRA.remove('/foo/bar', true);

                // Ensure the directory is no longer there
                const filesAfter = await mediaRA.list('/foo/bar');
                expect(filesAfter).to.be.an.instanceOf(Array);
                expect(filesAfter).to.have.length(0);

                // Ensure an actual record is there
                const existsAfter = await mediaRA.exists('/foo/bar/sparse.txt');
                expect(existsAfter).to.equal(false);
            });
        });

        describe('Sparse item handling', () =>
        {
            it('removes the directory and its contents if recursive is passed', async () =>
            {
                // Ensure the directory is there, with contents
                const files = await mediaRA.list('/foo/nested');
                expect(files).to.be.an.instanceOf(Array);
                expect(files).to.have.length(4);

                // Remove the directory
                await mediaRA.remove('/foo/nested', true);

                // Ensure the directory is no longer there
                const after = await mediaRA.list('/foo/nested');
                expect(after).to.be.an.instanceOf(Array);
                expect(after).to.have.length(0);
            });
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
