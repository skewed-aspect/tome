// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the MediaDBResourceAccess module.
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';
import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';

// Utils
import dbUtil from '../../../src/server/utils/database';

// Resource Access
import wikiRA from '../../../src/server/resource-access/wiki';
import { NotFoundError } from '../../../src/server/errors';

// ---------------------------------------------------------------------------------------------------------------------

chai.use(chaiAsPromised);

// ---------------------------------------------------------------------------------------------------------------------

describe('Wiki Resource Access', () =>
{
    let db : Knex;

    beforeEach(async () =>
    {
        db = await dbUtil.getDB('wiki');

        // Load the test cases
        await db('wiki_rev')
            .insert([
                {
                    rev_id: 'rev1', path: '/welcome', status: 'published', title: 'Welcome!', headline: 'Landing Page',
                    tags: '[]', contents: "# Welcome!\n\nThis is the default welcome page.",
                    created: db.raw('datetime(\'now\', \'-20000 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev2', path: '/welcome', status: 'published', title: 'Welcome!', headline: 'Meh.',
                    tags: '["welcome","newbies","default page"]',
                    contents: "# Yo, Dawg\n\nThis is the default welcome page. It's fine.",
                    created: db.raw('datetime(\'now\', \'-19500 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev3', path: '/deleted', status: 'published', title: 'Deleted Page', headline: 'TODO',
                    tags: '["to-delete"]', contents: "This page exists to be deleted.",
                    created: db.raw('datetime(\'now\', \'-19500 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev4', path: '/deleted', status: 'deleted', title: '', headline: '', tags: '[]',
                    contents: "", created: db.raw('datetime(\'now\', \'-19000 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev5', path: '/deleted/drafts', status: 'published', title: 'Deleted Page 2',
                    headline: 'TODO', tags: '["to-delete"]', contents: "to be deleted.",
                    created: db.raw('datetime(\'now\', \'-19100 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev6', path: '/deleted/drafts', status: 'deleted', title: '', headline: '', tags: '[]',
                    contents: "", created: db.raw('datetime(\'now\', \'-19050 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev7', path: '/deleted/drafts', status: 'draft', title: 'Brought back',
                    headline: 'Hey, it could happen', tags: '["miracle", "draft-test"]',
                    contents: "And now we're back! Maybe.", created: db.raw('datetime(\'now\', \'-19000 seconds\')'),
                    account: 'unknown'
                },
                {
                    rev_id: 'rev8', path: '/drafts', status: 'published', title: 'Draft Test Page',
                    headline: 'draft', tags: '["draft-test"]', contents: "testing drafts",
                    created: db.raw('datetime(\'now\', \'-19100 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev9', path: '/drafts', status: 'draft', title: 'Testing Drafts',
                    headline: 'Do they work?', tags: '["draft-test"]',
                    contents: "Drafts work! Maybe.", created: db.raw('datetime(\'now\', \'-19000 seconds\')'),
                    account: 'unknown'
                },
                {
                    rev_id: 'rev9a', path: '/drafts', status: 'draft', title: 'Testing Drafts II',
                    headline: 'Even more edits', tags: '["draft-test"]',
                    contents: "They seem to work.", created: db.raw('datetime(\'now\', \'-18000 seconds\')'),
                    account: 'unknown'
                },
                {
                    rev_id: 'rev10', path: '/drafts/only', status: 'draft', title: 'Testing Only Drafts',
                    headline: '...there\'s a joke here...', tags: '["draft-test"]',
                    contents: "Nyan?", created: db.raw('datetime(\'now\', \'-19100 seconds\')'), account: 'unknown'
                },
                {
                    rev_id: 'rev11', path: '/drafts/only', status: 'draft', title: 'Testing Only Drafts',
                    headline: '...there\'s a joke here...', tags: '["draft-test"]',
                    contents: "Ok, no, so real content now",
                    created: db.raw('datetime(\'now\', \'-18000 seconds\')'), account: 'unknown'
                },
            ]);

        await db('wiki_page')
            .insert([
                { path: '/welcome', latest_rev: 'rev2', created: db.raw('datetime(\'now\', \'-20000 seconds\')') },
                { path: '/deleted', latest_rev: 'rev4', created: db.raw('datetime(\'now\', \'-19500 seconds\')') },
                { path: '/drafts', latest_rev: 'rev8', created: db.raw('datetime(\'now\', \'-19100 seconds\')') },
                { path: '/drafts/only', latest_rev: 'rev10', created: db.raw('datetime(\'now\', \'-19100 seconds\')') },
                { path: '/deleted/drafts', latest_rev: 'rev6', created: db.raw('datetime(\'now\', \'-19100 seconds\')') },
            ]);
    });

    afterEach(async () =>
    {
        // Clear the database
        await db('wiki_rev').truncate();
        await db('wiki_page').truncate();
    });

    describe('exists', () =>
    {
        it('returns true if there is a published page at the given path', async () =>
        {
            const exists = await wikiRA.exists('/welcome');
            expect(exists).to.equal(true);
        });

        it('returns false if the latest revision is a deleted revision', async () =>
        {
            const exists = await wikiRA.exists('/deleted');
            expect(exists).to.equal(false);
        });

        it('ignores draft revisions', async () =>
        {
            const exists = await wikiRA.exists('/deleted/drafts');
            expect(exists).to.equal(false);

            const exists2 = await wikiRA.exists('/drafts/only');
            expect(exists2).to.equal(false);
        });
    });

    describe('get', () =>
    {
        it('returns the latest published revision', async () =>
        {
            const page = await wikiRA.get('/welcome');
            expect(page).to.have.property('baseRevID', 'rev2');
            expect(page).to.have.property('path', '/welcome');
            expect(page).to.have.property('status', 'published');
            expect(page).to.have.property('title', 'Welcome!');
            expect(page).to.have.property('headline', 'Meh.');
            expect(page).to.have.property('contents', "# Yo, Dawg\n\nThis is the default welcome page. It's fine.");

            expect(page).to.have.property('tags');
            expect(page.tags).to.be.an('array');
            expect(page.tags).to.include('welcome');
            expect(page.tags).to.include('newbies');
            expect(page.tags).to.include('default page');
        });

        it('throws a NotFoundError if the page does not exist', async () =>
        {
            await expect(wikiRA.get('/dne')).to.rejectedWith(NotFoundError);
        });

        it('returns a page with a deleted status if the page is deleted', async () =>
        {
            const page = await wikiRA.get('/deleted');
            expect(page).to.have.property('baseRevID', 'rev4');
            expect(page).to.have.property('path', '/deleted');
            expect(page).to.have.property('status', 'deleted');
            expect(page).to.have.property('title', '');
            expect(page).to.have.property('headline', '');
            expect(page).to.have.property('contents', '');

            expect(page).to.have.property('tags');
            expect(page.tags).to.be.an('array').that.is.empty;
        });

        describe('`includeDrafts` option', () =>
        {
            it('ignores drafts when the option is not passed', async () =>
            {
                const page = await wikiRA.get('/deleted/drafts');
                expect(page).to.have.property('baseRevID', 'rev6');
                expect(page).to.have.property('status', 'deleted');
                expect(page).to.have.property('contents', '');

                const draft = await wikiRA.get('/drafts');
                expect(draft).to.have.property('baseRevID', 'rev8');
                expect(draft).to.have.property('status', 'published');
                expect(draft).to.have.property('contents', 'testing drafts');
            });

            it('throws a NotFoundError if the page only has drafts', async () =>
            {
                await expect(wikiRA.get('/drafts/only')).to.be.rejectedWith(NotFoundError);
            });

            it('returns the latest draft revision, if any', async () =>
            {
                const draft = await wikiRA.get('/drafts', true);
                expect(draft).to.have.property('baseRevID', 'rev9a');
                expect(draft).to.have.property('status', 'draft');
                expect(draft).to.have.property('title', 'Testing Drafts II');
                expect(draft).to.have.property('contents', 'They seem to work.');

                const draft2 = await wikiRA.get('/drafts/only', true);
                expect(draft2).to.have.property('baseRevID', 'rev11');
                expect(draft2).to.have.property('status', 'draft');
                expect(draft2).to.have.property('title', 'Testing Only Drafts');
                expect(draft2).to.have.property('contents', 'Ok, no, so real content now');
            });

            it('returns the latest published version if there is no drafts', async () =>
            {
                const page = await wikiRA.get('/welcome', true);
                expect(page).to.have.property('baseRevID', 'rev2');
                expect(page).to.have.property('path', '/welcome');
                expect(page).to.have.property('status', 'published');
                expect(page).to.have.property('title', 'Welcome!');
                expect(page).to.have.property('headline', 'Meh.');
                expect(page).to.have.property('contents', "# Yo, Dawg\n\nThis is the default welcome page. It's fine.");

                expect(page).to.have.property('tags');
                expect(page.tags).to.be.an('array');
                expect(page.tags).to.include('welcome');
                expect(page.tags).to.include('newbies');
                expect(page.tags).to.include('default page');
            });
        });
    });

    describe('getRevisions', () =>
    {
        it('returns all revisions for the given path', async () =>
        {
            const revs = await wikiRA.getRevisions('/welcome');
            expect(revs).to.be.an('array');
            expect(revs).to.have.length(2);
            expect(revs[0]).to.have.property('revID', 'rev2');
            expect(revs[1]).to.have.property('revID', 'rev1');

            const revs2 = await wikiRA.getRevisions('/drafts');
            expect(revs2).to.be.an('array');
            expect(revs2).to.have.length(3);
            expect(revs2[0]).to.have.property('revID', 'rev9a');
            expect(revs2[1]).to.have.property('revID', 'rev9');
            expect(revs2[2]).to.have.property('revID', 'rev8');

            const revs3 = await wikiRA.getRevisions('/drafts/only');
            expect(revs3).to.be.an('array');
            expect(revs3).to.have.length(2);
            expect(revs3[0]).to.have.property('revID', 'rev11');
            expect(revs3[1]).to.have.property('revID', 'rev10');
        });

        it('throws a NotFoundError if no page exists at the path', async () =>
        {
            await expect(wikiRA.getRevisions('/dne')).to.rejectedWith(NotFoundError);
        });
    });

    describe('set', () =>
    {
        it('adds new wiki pages if no page currently exists at the given path', async () =>
        {
            const newPage = {
                baseRevID: null,
                status: 'published' as const,
                title: 'New Page Test',
                headline: 'New page, who dis?',
                tags: [ 'new-page', 'test' ],
                contents: 'Testing out a new page'
            }

            // Insert new page
            await wikiRA.set('/new-page', newPage, 'unknown')

            // Check the page exists
            const page = await wikiRA.get('/new-page');
            expect(page).to.have.property('baseRevID');
            expect(page).to.have.property('path', '/new-page');
            expect(page).to.have.property('status', 'published');
            expect(page).to.have.property('title', newPage.title);
            expect(page).to.have.property('headline', newPage.headline);
            expect(page).to.have.property('contents', newPage.contents);

            expect(page).to.have.property('tags');
            expect(page.tags).to.be.an('array');
            expect(page.tags).to.include('new-page');
            expect(page.tags).to.include('test');

            // Check it has only one revision.
            const revs = await wikiRA.getRevisions('/new-page');
            expect(revs).to.be.an('array');
            expect(revs).to.have.length(1);
        });

        it('updates the wiki page if a page already exists at the given path', async () =>
        {
            const updatePage = {
                baseRevID: 'rev2',
                status: 'published' as const,
                title: 'Update Test',
                headline: 'new words here',
                tags: [ 'new-page', 'test' ],
                contents: 'updated test'
            }

            // Insert new page
            await wikiRA.set('/welcome', updatePage, 'unknown')

            // Check the page updated
            const page = await wikiRA.get('/welcome');
            expect(page).to.have.property('path', '/welcome');
            expect(page).to.have.property('status', 'published');
            expect(page).to.have.property('title', updatePage.title);
            expect(page).to.have.property('headline', updatePage.headline);
            expect(page).to.have.property('contents', updatePage.contents);

            expect(page).to.have.property('tags');
            expect(page.tags).to.be.an('array');
            expect(page.tags).to.include('new-page');
            expect(page.tags).to.include('test');
        });

        it('saves a draft as the initial latest revision, if the page is new.', async () =>
        {
            const newPage = {
                baseRevID: null,
                status: 'draft' as const,
                title: 'New Page Test',
                headline: 'New page, who dis?',
                tags: [ 'new-page', 'test' ],
                contents: 'Testing out a new page'
            }

            // Insert new page
            await wikiRA.set('/new-page', newPage, 'unknown')

            // Check the page exists
            const page = await wikiRA.get('/new-page', true);
            expect(page).to.have.property('baseRevID');
            expect(page).to.have.property('path', '/new-page');
            expect(page).to.have.property('status', 'draft');
            expect(page).to.have.property('title', newPage.title);
            expect(page).to.have.property('headline', newPage.headline);
            expect(page).to.have.property('contents', newPage.contents);

            expect(page).to.have.property('tags');
            expect(page.tags).to.be.an('array');
            expect(page.tags).to.include('new-page');
            expect(page.tags).to.include('test');

            // Check it has only one revision.
            const revs = await wikiRA.getRevisions('/new-page');
            expect(revs).to.be.an('array');
            expect(revs).to.have.length(1);
        });

        it('does not update the latest revision when saving drafts', async () =>
        {
            const updatePage = {
                baseRevID: 'rev2',
                status: 'draft' as const,
                title: 'Update Test',
                headline: 'new words here',
                tags: [ 'new-page', 'test' ],
                contents: 'updated test'
            }

            // Insert new page
            await wikiRA.set('/welcome', updatePage, 'unknown')

            // Check the page updated
            const page = await wikiRA.get('/welcome');
            expect(page).to.have.property('path', '/welcome');
            expect(page).to.have.property('status', 'published');
            expect(page).to.have.property('title', 'Welcome!');
            expect(page).to.have.property('headline', 'Meh.');
            expect(page).to.have.property('contents', "# Yo, Dawg\n\nThis is the default welcome page. It's fine.");

            expect(page).to.have.property('tags');
            expect(page.tags).to.be.an('array');
            expect(page.tags).to.include('welcome');
            expect(page.tags).to.include('newbies');
            expect(page.tags).to.include('default page');
        });
    });

    describe('discardDrafts', () =>
    {
        it('discards all drafts created since the latest revision', async () =>
        {
            // Get the current revision count.
            const revs = await wikiRA.getRevisions('/deleted/drafts');
            expect(revs).to.have.length(3);

            // Discard drafts
            await wikiRA.discardDrafts('/deleted/drafts');

            // Get the current revision count.
            const newRevs = await wikiRA.getRevisions('/deleted/drafts');
            expect(newRevs).to.have.length(2);
        });

        it('does something magical when called on a page that only has drafts.', async () =>
        {
            // Get the current revision count.
            const revs = await wikiRA.getRevisions('/drafts/only');
            expect(revs).to.have.length(2);

            // Discard drafts
            await wikiRA.discardDrafts('/drafts/only');

            // Get the current revision count.
            const newRevs = await wikiRA.getRevisions('/drafts/only');
            expect(newRevs).to.have.length(1);
        });

        it('does nothing if there are no drafts', async () =>
        {
            // Get the current revision count.
            const revs = await wikiRA.getRevisions('/welcome');
            expect(revs).to.have.length(2);

            // Discard drafts
            await wikiRA.discardDrafts('/welcome');

            // Get the current revision count.
            const newRevs = await wikiRA.getRevisions('/welcome');
            expect(newRevs).to.have.length(2);
        });

        it('throws a NotFoundError if no page exists at the path', async () =>
        {
            await expect(wikiRA.discardDrafts('/dne')).to.rejectedWith(NotFoundError);
        });
    });

    describe('remove', () =>
    {
        it('adds a deleted revision to a published page', async () =>
        {
            // Delete page
            await wikiRA.remove('/welcome');

            const page = await wikiRA.get('/welcome');
            expect(page).to.have.property('path', '/welcome');
            expect(page).to.have.property('status', 'deleted');
            expect(page).to.have.property('title', '');
            expect(page).to.have.property('headline', '');
            expect(page).to.have.property('contents', '');

            expect(page).to.have.property('tags');
            expect(page.tags).to.be.an('array').that.is.empty;

            // Get the revision count.
            const revs = await wikiRA.getRevisions('/welcome');
            expect(revs).to.have.length(3);
        });

        it('does nothing if the page is already deleted', async () =>
        {
            // Get the current revision count.
            const revs = await wikiRA.getRevisions('/deleted');
            expect(revs).to.have.length(2);

            // Delete page
            await wikiRA.remove('/deleted');

            // Get the current revision count.
            const newRevs = await wikiRA.getRevisions('/deleted');
            expect(newRevs).to.have.length(2);
        });

        it('does nothing if no page exists at the path', async () =>
        {
            // Delete page
            await wikiRA.remove('/dne');

            // Make sure nothing exists
            const exists = await wikiRA.exists('/dne');
            expect(exists).to.equal(false);
        });

        it('does nothing if only draft revisions exist at the path', async () =>
        {
            // Delete page
            await wikiRA.remove('/drafts/only');

            // Retrieve the page
            const page = await wikiRA.get('/drafts/only', true);
            expect(page).to.have.property('baseRevID', 'rev11');
        });

        describe('`prune` option', () =>
        {
            it('deletes the page and all revisions if passed', async () =>
            {
                // Delete page
                await wikiRA.remove('/welcome', '', true);
                const results = await db('wiki_page').select().where({ path: '/welcome' });
                expect(results).to.be.an('array').with.lengthOf(0);

                // Delete page
                await wikiRA.remove('/drafts/only', '', true);
                const results2 = await db('wiki_page').select().where({ path: '/drafts/only' });
                expect(results2).to.be.an('array').with.lengthOf(0);

            });

            it('does nothing if the page does not exist at the path', async () =>
            {
                // Delete page
                await wikiRA.remove('/dne', '', true);

                // Make sure nothing exists
                const exists = await wikiRA.exists('/dne');
                expect(exists).to.equal(false);
            });
        });
    });

    describe('prune', () =>
    {
        it('only keeps the latest published revision', async () =>
        {
            // Get the current revision count.
            const revs = await wikiRA.getRevisions('/welcome');
            expect(revs).to.have.length(2);

            // Discard drafts
            await wikiRA.prune('/welcome');

            // Get the current revision count.
            const newRevs = await wikiRA.getRevisions('/welcome');
            expect(newRevs).to.have.length(1);
        });

        it('throws a NotFoundError if no page exists at the path', async () =>
        {
            await expect(wikiRA.prune('/dne')).to.be.rejectedWith(NotFoundError);
        });

        describe('`discard` option', () =>
        {
            it('discards all drafts, if true', async () =>
            {
                // Get the current revision count.
                const revs = await wikiRA.getRevisions('/deleted/drafts');
                expect(revs).to.have.length(3);

                // Discard drafts (turn off purge)
                await wikiRA.prune('/deleted/drafts', true, false);

                // Get the current revision count.
                const newRevs = await wikiRA.getRevisions('/deleted/drafts');
                expect(newRevs).to.have.length(1);
            });

            it('keeps drafts and the last published or deleted revision if false', async () =>
            {
                // Get the current revision count.
                const revs = await wikiRA.getRevisions('/deleted/drafts');
                expect(revs).to.have.length(3);

                // Discard drafts (turn off purge)
                await wikiRA.prune('/deleted/drafts', false, false);

                // Get the current revision count.
                const newRevs = await wikiRA.getRevisions('/deleted/drafts');
                expect(newRevs).to.have.length(2);
            });
        });

        describe('`purge` option', () =>
        {
            it('deletes the page and all revisions of deleted pages if true', async () =>
            {
                // Get the current revision count.
                const revs = await wikiRA.getRevisions('/deleted');
                expect(revs).to.have.length(2);

                // Discard drafts
                await wikiRA.prune('/deleted');

                const results = await db('wiki_page').select().where({ path: '/deleted' });
                expect(results).to.be.an('array').with.lengthOf(0);
            });

            it('deletes the page and all revisions if true, even if there are drafts.', async () =>
            {
                // Get the current revision count.
                const revs = await wikiRA.getRevisions('/deleted/drafts');
                expect(revs).to.have.length(3);

                // Discard drafts
                await wikiRA.prune('/deleted/drafts');

                const results = await db('wiki_page').select().where({ path: '/deleted/drafts' });
                expect(results).to.be.an('array').with.lengthOf(0);
            });

            it('keeps a deleted revision if purge is false', async () =>
            {
                // Get the current revision count.
                const revs = await wikiRA.getRevisions('/deleted');
                expect(revs).to.have.length(2);

                // Discard drafts
                await wikiRA.prune('/deleted', true, false);

                const revs2 = await wikiRA.getRevisions('/deleted');
                expect(revs2).to.have.length(1);
            });
        });
    });

    describe('revert', () =>
    {
        it('copies the specified revision to a new revision and makes that the latest', async () =>
        {
            await wikiRA.revert('/welcome', 'rev1');

            const page = await wikiRA.get('/welcome');
            expect(page).to.have.property('contents', '# Welcome!\n\nThis is the default welcome page.');
            expect(page.baseRevID).to.not.equal('rev1');
        });

        it('allows drafts to be reverted, without effecting the published page', async () =>
        {
            await wikiRA.revert('/drafts', 'rev9');

            const currentPage = await wikiRA.get('/drafts');
            expect(currentPage).to.have.property('baseRevID', 'rev8');

            const draft = await wikiRA.get('/drafts', true);
            expect(draft).to.have.property('status', 'draft');
            expect(draft).to.have.property('title', 'Testing Drafts');
            expect(draft).to.have.property('contents', 'Drafts work! Maybe.');
        });
    });
});
