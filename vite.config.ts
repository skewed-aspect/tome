// ---------------------------------------------------------------------------------------------------------------------

import path from 'node:path';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import dotenv from 'dotenv';

import ConfigUtil from './src/server/utils/config';
import type { TomeConfig } from './src/interfaces/config';

//----------------------------------------------------------------------------------------------------------------------
// Configuration
//----------------------------------------------------------------------------------------------------------------------

dotenv.config();
ConfigUtil.loadConfigFile();
const config = ConfigUtil.getConfig<TomeConfig>();

// ---------------------------------------------------------------------------------------------------------------------

export default defineConfig({
    root: 'src/client',
    plugins: [ vue() ],
    server: {
        port: config.server.port,
        proxy: {
            '/api': `http://localhost:${ config.server.port - 1 }`,
            '/session': `http://localhost:${ config.server.port - 1 }`,
            '/version': `http://localhost:${ config.server.port - 1 }`,
            '/socket.io': {
                target: `http://localhost:${ config.server.port - 1 }`,
                ws: true
            }
        },
        https: false,
        open: false
    },
    resolve: {
        alias: {
            '@': path.resolve('src', 'client')
        }
    },
    build: {
        outDir: '../../dist/client',
        emptyOutDir: true,
        cssCodeSplit: true,
        // chunkSizeWarningLimit: 650,
        // rollupOptions: {
        //     output: {
        //         manualChunks: {
        //             bootstrap: [ 'popper.js', 'jquery', 'bootstrap' ],
        //             bootstrapVue: [ 'bootstrap-vue' ]
        //         }
        //     }
        // }
    }
});

// ---------------------------------------------------------------------------------------------------------------------
