// ---------------------------------------------------------------------------------------------------------------------
// Knex CLI Config
// ---------------------------------------------------------------------------------------------------------------------

import dotenv from 'dotenv';

// Utils
import dbUtil from './src/server/utils/database';
import ConfigUtil from './src/server/utils/config';

//----------------------------------------------------------------------------------------------------------------------
// Configuration
//----------------------------------------------------------------------------------------------------------------------

dotenv.config();
ConfigUtil.loadConfigFile();

// ---------------------------------------------------------------------------------------------------------------------

export default async function buildConfig()
{
    const mediaCfg = await dbUtil.getDBConfig('media');
    const wikiCfg = await dbUtil.getDBConfig('media');

    return {
        media: {
            ...mediaCfg,
            migrations: {
                ...await dbUtil.getMigrationConfig('media')
            }
        },
        wiki: {
            ...wikiCfg,
            migrations: {
                ...await dbUtil.getMigrationConfig('wiki')
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
